2008-06-07  Kjartan Maraas  <kmaraas@gnome.org>

	* configure.in: Add «nb» to ALL_LINGUAS.

2007-10-14  Yannig Marchegay  <yannig@marchegay.org>

	* configure.in: Added 'oc' to ALL_LINGUAS.

2007-08-17  Raivis Dejus  <orvils@gmail.com>

        * configure.in: Added Latvian Translation (lv).

2006-12-13  Pema Geyleg  <pgeyleg@gmail.com>

	* configure.in: Added 'dz' in ALL_LINGUAS

2006-02-12  Clytie Siddall <clytie@riverland.net.au>

	* configure.in	Added vi in ALL_LINGUAS line.
	
	2005-09-21  Andrej Kacian <andrej@kacian.sk>
	* src/servers.c, src/servers.h, src/silccallbacks.c,
	  src/ui_preferences.c, src/xmllayout.c, src/xmllayout.h, silky.glade:
	  Support passphrase-protected servers.

2005-08-23  Andrej Kacian <andrej@kacian.sk>
	* src/silc-notify.c: Handle channel topic changes made by channel and
	  server.

2005-07-15  pawan Chitrakar  <pawan@nplinux.org>

	* configure.in: Added ne Nepali in ALL_LINGUAS

2005-06-06  Andrej Kacian <andrej@kacian.sk>
	* Operate on correct item instead of previously selected one when
	  right-clicking on channel nicklist and buddylist

2005-06-03  Andrej Kacian <andrej@kacian.sk>
	* Rewrote buddylist treeview layout to look more usable
	* Write backups of buddies.conf and user's servers.conf upon succesful
	  load

2005-05-25  Toni Willberg <toniw@iki.fi>
	* made userinfo dialog modal
	* code cleanup
	* buddylist is now refreshed more often

	* connection progress banner fixed, now updates untill connection is made

	* status bar shown only when there's something to show
	* doesn't complain about missing mime.types anymore
	* made GtkNotebook tabs left-aligned

2005-05-24  Andrej Kacian <andrej@kacian.sk>
	* added menu for presence status
	* insensitivize most presence states in menu when offline
	* catch SIGINT, displaying disconnection dialog if connected

2005-05-21  Toni Willberg <toniw@iki.fi>
	* now shows an error dialog if user gave wrong passphrase

2005-05-20  Toni Willberg <toniw@iki.fi>
	* made the credits dialog scrollable
	* made the preferences dialog a window,
	  and added scrolls to all tabs of the preferences

2005-05-19  Toni Willberg <toniw@iki.fi>
	* added progress dialog for connecting server
	* progress dialogs can't be closed by user anymore
	* added new function for scheduling a window hiding
	* added error dialog for server connection failure

	* new functions for showing error dialogs
	* code cleanup

2005-05-17  Toni Willberg <toniw@iki.fi>
	* changed server connecting to use threads; silc library blocks, 
	  which causes bad usability experience on slow machines
	* added a dialog to inform about unsuccessful connect attempt
	* new functions to set connection status 
	  (set_connection_disconnected/connected/disconnecting/connecting)
	* cleanups

	* fixed a bug in server connection dialog: entries were added twice
	  if dialog was opened, closed and reopened

2005-05-12  Toni Willberg <toniw@iki.fi>
	* Fixed warnings, mostly builds cleanly with GCC 4.0 now
	* Fixed a crasher bug in gui.c: crashed on connect dialog
	* Fixed a bug that prevented site-wide server list being ever used

2005-04-22  Toni Willberg <toniw@iki.fi>
	* using guint instead of uint, fixes build inside Scratchbox
	  Affected files: query.h, commands.c

2005-04-17  Christopher Orr  <chris@orr.me.uk>

        * configure.in: Added en_GB to ALL_LINGUAS.

2005-04-07  Andrej Kacian  <andrej@kacian.sk>
	* Made Silky compatible with silc-toolkit 0.9.13

2005-04-02  Priit Laes  <amd@store20.com>

	* replaced deprecated GtkCombo with GtkComboBoxEntry
	
2005-04-01  Steve Murphy  <murf@e-tools.com>

        * configure.in: Added "rw" to ALL_LINGUAS.

2005-04-01  Raphael Higino  <raphaelh@cvs.gnome.org>

	* configure.in: Added pt_BR to ALL_LINGUAS.

2005-03-27 Toni Willberg <toniw@iki.fi>
	* buddylist now shown on start
	* replaced the console tab with buddylist
	* added groups to buddylist

2005-03-19 Toni Willberg <toniw@iki.fi>
	=====================
	* Silky 0.5.4 release
	=====================

2005-03-19 Toni Willberg <toniw@iki.fi>
	* fixes for win32 build from Juha Räsänen:
	  - fixes to build and run the Win32 version Cygwin+MinGW
	  - query.c|h: changed uint to guint
	  - fixed /list crashes when channel has no topic
	  - servers.con, silky.conf and tips.txt located now
	    in C:\Program Files\Silky\data

	* new silky.glade file committed, fixes runtime warnings
	* updated credits list

2005-03-17 Toni Willberg <toniw@iki.fi>
	=====================
	* Silky 0.5.3 release
	=====================

2005-03-17 Toni Willberg <toniw@iki.fi>
	* Removed silky.spec. The official silky.spec is now available
	  from cvs.fedora.redhat.com. Use it instead if you want to
	  package Silky for some other RPM-based distro.


2005-03-17 Andrej Kacian <andrej@kacian.sk>
	* Added a "Show user info" button to invitation confirmation dialog.

2005-03-07 Toni Willberg <toniw@iki.fi>
	* Moved CVS from Sourceforge to GNOME.org

2005-03-05 Andrej Kacian <andrej@kacian.sk>
	* Implemented ctrl-j (join channel), ctrl-n (change nickname) and
	  ctrl-w (close current tab, leave SILC channel if applicable)

2005-03-02 Toni Willberg <toniw@iki.fi>
	* Enabled the tips setting in preferences
	* Enabled the user information dialog

2005-02-27 Andrej Kacian <andrej@kacian.sk>
	* Added dialog to handle incoming channel invitations
	* Wrote a generic function for channel join and used it everywhere

2005-01-29 Toni Willberg <toniw@iki.fi>
	* New About and Credits -dialogs
	* now looks for SILC client libraries with pkg-config, or alternatively takes them from
	  --with-silc-libs and --with-silc-include paths

2005-01-25 Toni Willberg <toniw@iki.fi>
	* Code cleanup in include files

2005-01-22 Andrej Kacian <andrej@kacian.sk>
	* Fixed port number loading in parse_servers()

2005-01-21 Toni Willberg <toniw@iki.fi>
	* passphrase can now be changed from preferences
	* minor UI cleaning

2005-01-20 Toni Willberg <toniw@iki.fi>
	* removed obsolete dialogs
	* adjusted icon position in all dialogs

2005-01-19 Toni Willberg <toniw@iki.fi>
	* new default colors
	* server ports are now configurable in preferences

2005-01-14 Andrej Kacian <andrej@kacian.sk>
	* Silky now verifies signatures of signed channel messages

2005-01-14 Toni Willberg <toniw@iki.fi>
	* Changed the color preferences UI to use GtkColorButtons and color selector
	* Now using GtkFontButton for font selection
	* Changed the channel settings tab to use GtkExpander
	* Changed position of channel close -button to right side of the topic

2005-01-09 Toni Willberg <toniw@iki.fi>
	* Changed WATCH to use public key instead of nickname

2005-01-07 Toni Willberg <toniw@iki.fi>
	* the debugging system now supports levels: LOG_ERROR, LOG_MESSAGE, LOG_INFO and LOG_DEBUG
	* new debug functions are: debug_error(), debug_info(), debug_message() and debug()
	* complemented --help output texts

2005-01-05 Toni Willberg <toniw@iki.fi>
	* implemented "/WHOIS -pubkey filename" command to do whois based on pubkey file
	* implemented WHOIS into buddylist (uses public key)
	* started to draw documentation of Silky's states (states.dia)
	* changed the PING functionality: now PING is sent to server only if there has been
	  no notifies or command replies for N seconds

2004-12-14 Toni Willberg <toniw@iki.fi>
	* SilkyBuddy pointer b stored into the buddylist UI for use in callbacks
	* more cleanups

2004-12-12 Toni Willberg <toniw@iki.fi>
	* Moved console tab related functions to ui_console.c/h
	* removed unused variables
	* fixed incorrect usage of strlen() in main.c
	* added ifndef/define/endif to header files where they were missing
	* added some missing #includes which were found after ifndef magic :)
	* renamed silc_ -> silky_save_public_key to avoid confusion
	* added silky_known_key()
	* moved ui_buddy_remove() to ui_buddylist.h
	* fixed memory leaks in watch_del() and watch_add (buddylist.c)
	* Added README.internals (describes how Silky works internally)
	* Adding a buddy now calls GETKEY
	* adding "keyverified" parameter to buddies.conf (0=unverified, 1=verified)

	* code cleanup in keys.c

2004-12-01 Toni Willberg <toniw@iki.fi>
	* Fixed /MSG command. Now it refuses to send the message if nickname matches to
	  multiple persons.
	* Fixed /LIST, doesn't show non-utf8 topics anymore. Fixes crash on Windows.
	* Fixes window placement problem, doesn't center on resize anymore.

2004-10-24 Toni Willberg <toniw@iki.fi>
	* Implemented /CLEAR

=========================================================================================

2004-10-09 Toni Willberg <toniw@iki.fi>
	* Released Silky 0.5.2

2004-10-09 Toni Willberg <toniw@iki.fi>
	* preparing for release 0.5.2
	* channel nicklist is now being greyed out (insensitive) on disconnect,
	  also fixed a runtime warning related to this
	* new list of default servers in use
	* changed some icons to gain Gtk2.2 compatibility
	* fixed a string in the preferences dialog
	* disabled unnecessary tablist popup
	* fixed word wrapping
	* added gettext-devel for buildrequires in silky.spec for Fedora

2004-10-08 Andrej Kacian <andrej@kacian.sk>
	* Handle server timeout during connecting

2004-09-24 Toni Willberg <toniw@iki.fi>
	* Disabled the Tips for now (they were not translatable)
	* Fixed setting realname
	* Disabled non-implemented stuff from the preferences
	* preparing for 0.5.2 release

2004-09-20 Andrej Kacian <andrej@kacian.sk>
	* Version bump to 0.5.2pre2 (for TP)

2004-09-06 Andrej Kacian <andrej@kacian.sk>
	* Add tip of the day support on startup, with few tips in tips.txt

2004-09-05 Toni Willberg <toniw@iki.fi>
	* files.h provides DIR_SEPARATOR defined (%c), which should be used as a directory
	  separator character in Silky. Provides win32 compat.
	* key.c now uses DIR_SEPARATOR instead of ifdef/else stuff for win32
	* fixed possible corrupted filename problem in keys.c (missing memset for path3)

2004-09-04 Toni Willberg <toniw@iki.fi>
	* Implemented GETKEY command

2004-09-04 Toni Willberg <toniw@iki.fi>
	* Created ui_buddylist.c/h files, and moved all buddylist+gui related
	  stuff there
	* Created ui_channel.c/h, moved stuff there from other modules
	* Removed buddylist.h and channel.h from includes.h, and added some #ifndef stuff
	  to various .h files.

2004-09-04 Andrej Kacian <andrej@kacian.sk>
	* Fixed Gtk-CRITICAL on own nick change (closes #942148 and #954920)
	* Fixed server disconnect when sending "/" from command line (patch
	  by Scott Garman, closes #983573)
	* Fixed a bug where only last added server would get saved to
	  servers.conf

2004-09-03 Andrej Kacian <andrej@kacian.sk>
	* Nicklist can now be custom-sorted, re-sorting after any change.

2004-08-31 Andrej Kacian <andrej@kacian.sk>
	* Do not focus query tab when new text arrives
	* Query tabs now also get notification when new text arrives

2004-07-11 Toni Willberg <toniw@iki.fi>
	* Servers can now be added and removed via preferences

2004-07-11 Andrej Kacian <andrej@kacian.sk>
	* Wrote servers_store_to_xml()
	* Re-added server fingerprint to servers.conf
	* Fixed some gtk-doc entries syntax

2004-06-29 Toni Willberg <toniw@iki.fi>
	* now using word wrap instead of character wrap again
	* text autoscrolls now only if we are at the last line before
	  printing the text
	* implemented server adding functionality to preferences (doesn't
	  yet save)

2004-06-22 Toni Willberg <toniw@iki.fi>
	* added silky_get_active_textview()
	* implemented page up/down keys to scroll text up/down

2004-06-12 Andrej Kacian <andrej@kacian.sk>
	* added server_remove()
	* fixed background color for nicklist (patch by Sebastian Spaeth,
	  closes sf.net bug #937491)

2004-05-17 Andrej Kacian <andrej@kacian.sk>
	* Fix incorrect activate handler function for join channel GtkEntry
	  (thanks to Sebastian Spaeth)

2004-04-25 Toni Willberg <toniw@iki.fi>
	* Applied RNG patch from Mika Boström
	* Implemented Buddy List functionality
	* Implemented WATCH command, handler and corresponding notify function
	* Fixed a crashing bug on invite

2004-04-22 Andrej Kacian <andrej@kacian.sk>
	* Wrote support for buddy list
	* Minor fixes to servers code
	* Fix to xml_write_config() when saving xmlDoc with multiple nodes with
	  same name

=========================================================================================

2004-04-18 Toni Willberg <toniw@iki.fi>
	* Released Silky 0.5.1

2004-04-18 Toni Willberg <toniw@iki.fi>
	* Moved lag'o'meter from console tab to main statusbar

2004-04-18 Andrej Kacian <andrej@kacian.sk>
	* Moved preference window server list columns set up to separate
	  function in gui.c
	* Moved preference window server list populating to prefs_populate_gui()
	  in preferences.c
	* Some code cleanup before release

2004-04-17 Andrej Kacian <andrej@kacian.sk>
	* Changed silkyStruct not to be pointer (gtk-doc required it)
	* Few other cosmetic changes for gtk-doc (see cvs log for details)
	* Do not build documentation from dist tarball (not included there)
	* Changed onoff_*() to prefs_*()

2004-04-17 Toni Willberg <toniw@iki.fi>
	* Tabs can be now changed by pressing alt+1..9
	* Tabs can also be changed by pressing alt+left/right
	* Nicklist colors are now set correctly
	* Wrapping by characters instead of words

2004-04-16 Andrej Kacian <andrej@kacian.sk>
	* Released Silky 0.5.1pre3

2004-04-16 Andrej Kacian <andrej@kacian.sk>
	* Rewrote and reenabled /set command
	* Fixed "Show tips..." checkbox functionality
	* Minor fixes to preferences system
	* Install default silky.conf to PREFIX/share/silky and use it if no
	  silky.conf is found in ~/.silky

2004-04-15 Andrej Kacian <andrej@kacian.sk>
	* Optimized GUI preferences populating and saving
	* Added a sanity check to server list loading

2004-04-14 Toni Willberg <toniw@iki.fi>
	* Removed debug from preferences dialog
	* Debug can now be turned on at runtime by using -d or --debug
	  parameters

2004-04-14 Andrej Kacian <andrej@kacian.sk>
	* Rewrote GUI preferences populating/saving (will need some
	  optimizations, but it's good to go)
	* Fixed a crash bug in topic inputbox
	* Removed 'type="..."' attribute from XML config, all values are now
	  stored as strings

2004-04-04 Andrej Kacian <andrej@kacian.sk>
	* Released Silky 0.5.1pre2

2004-04-11 Andrej Kacian <andrej@kacian.sk>
	* Some code cleanup
	* Complete non-glade translatable strings review and cleanup
	* Try to load site-wide servers.conf if none found in ~/.silky
	* Install default servers.conf to PREFIX/share/silky

2004-04-10 Andrej Kacian <andrej@kacian.sk>
	* Check that all channels and queries have correct tab number when a
	  tab is closed
	* Added functions for easy setting tab number for channels and queries

2004-04-09 Toni Willberg <toniw@iki.fi>
	* Fixed a bug causing crash in Win32 build
	* When browsing to the end of history with [PgDn] now appends
	  an empty line to the list.
	* Server list is now shown in the preferences window.
	* Size of the preferences dialog is now dynamic.
	* Removed option to select position of notebook tabs. They are
	  Now fixed to bottom.

2004-04-04 Toni Willberg <toniw@iki.fi>
	* Released Silky 0.5.1pre1

2004-04-03 Andrej Kacian <andrej@kacian.sk>
	* Fixed disconnect/quit behavior bug
	* Menu Connect and Disconnect options from Servers menu now get
	  properly enabled/disabled
	* Invalid password will get selected in initial password dialog on next
	  attempt
	* Similarly, entered text will be selected after user tries to send
	  it when not connected or when typing into console

2004-04-03 Toni Willberg <toniw@iki.fi>
	* Checked and fixed all strings in Silky.
	* Fixed the Gnome panel menu entry for Silky
	* Binary file sending is currently disabled. Will be re-enabled later.
	* Tab icons now tells the user if there's some action in a channel.

2004-03-31 Andrej Kacian <andrej@kacian.sk>
	* Moved all checkbox labels into checkbox widgets
	* Reworked quit/disconnect logic
	* Fixed channel topic inputbox sensitivity (closes sf.net's bug 882939)
	* Removed fingerprint and protocol fields from server config structure
	* Minor code cleanup

2004-03-29 Andrej Kacian <andrej@kacian.sk>
	* New channel is made active again on join

2004-03-27 Toni Willberg <toniw@iki.fi>
	* The servers.conf is now used in connect dialog
	* Nicknames are now formatted to show also the hostname if there are more
	  than one user with same nickname on same channel.
	* Changed preferences dialog to look like other dialogs.

2004-03-25 Daniel Richards <kyhwana@kyhwana.org
	* Added WIN32 code/ifdef for stat() (Windows doesn't have lstat()) and
	  fixed the silkyhomedir bug under WIN32

2004-03-25 Andrej Kacian <andrej@kacian.sk>
	* Silky now loads list of servers from a separate config file, which
	  is created, if not found.

2004-03-20 Andrej Kacian <andrej@kacian.sk>
	* Started work on the new print system - silky_print() in gui.c
	* Also free list of channel members when removing channel(s)

2004-03-18 Andrej Kacian <andrej@kacian.sk>
	* Applied a patch by Mika Bostrom for checking server's public key
	  upon connecting
	* Modified config system so that it supports multiple config files
	* Unknown node attributes are now removed correctly
	* Multiple smaller fixes to the config system

2004-03-16 Toni Willberg <toniw@iki.fi>
	* SILC Toolkit/Client library 0.9.12 now required, using old version causes
	  crashes for now on.

2004-03-14 Toni Willberg <toniw@iki.fi>
	* Removed silky-channel-label.glade.
	* Major code cleanup related to storing channel info internally.

2004-03-13 Andrej Kacian <andrej@kacian.sk>
	* Typedef-ed these structs - channel_t, chmember_t, cmd_history_t
	* Fixed glade widget name calling error in gui.c

2004-03-12 Toni Willberg <toniw@iki.fi>
	* Moved stuff from GtkWindows to GtkDialogs and designed common
	  layout for dialogs
	* Fixed bugs in thread code that caused hangs
	* Fixed bug about not finding SILC libraries on some platforms

2004-03-11 Toni Willberg <toniw@iki.fi>
	* Major code cleanup in main.c, complete rewrite of initialization code
	* Created many new .c files and moved code to them from (too) large files

2004-03-11 Andrej Kacian <andrej@kacian.sk>
	* Channel members list is now updated in a new way also on user kick,
	  user leave, cumode change and user signoff
	* Removed some redundant debug output from channel.c
	* Moved GUI usercount label update to refresh_gui_nicklist()
	* Channel members lists are now reordered properly (alphabetically) on
	  user nick change, if needed
	* Moved xml_elements[] and xml_attributes[] to a separate file
	* Added GUI buttons for signing channel/private actions/messages, all
	  except private messages are fully functional

2004-03-10 Andrej Kacian <andrej@kacian.sk>
	* On join, list of channel members is now stored into memory first,
	  only after that is GUI nicklist updated from the linked list
	* Added GNU headers to new .c files

2004-03-08 Toni Willberg <toniw@iki.fi>
	* Added "Tip of the Day"

2004-03-06 Toni Willberg <toniw@iki.fi>
	* Created new dialogs for "first run" key generation.
	* Added default option to generate keys without user interaction using
	  default values.
	* SILC key generation is now done in a thread.
	* Added a progress bar to show key generation activity.

=========================================================================================

2004-03-04 Silky Team
	* Silky Released 0.5.0!

2004-02-28 Toni Willberg <toniw@iki.fi>
	* Added channel settings tab
	* Invites are now shown to the user

2004-02-26 Andrej Kacian <andrej@kacian.sk>
	* Config file is now saved using our own routine, with desired
	  formatting

2004-02-23 Andrej Kacian <andrej@kacian.sk>
	* Unknown attributes are now removed from the config too
	* Fixed a segfaulting bug in config_save()

2004-02-22 Andrej Kacian <andrej@kacian.sk>
	* refresh_nicklist() gets called after a nick change too (fixes a
	  segfault)
	* /set command works again, though changed values do not show in GUI,
	  and there is no way to save them as of yet

2004-02-22 Toni Willberg <toniw@iki.fi>
	* User is now notified when non-active tab gets activity
	* Moved [X] buttons from channel tabs to single location to avoid
	  accidental clicks

2004-02-21 Andrej Kacian <andrej@kacian.sk>
	* Unknown nodes are now removed from the config just after we parse the
	  file

2004-02-20 Andrej Kacian <andrej@kacian.sk>
	* User is now properly removed from channel members list when leaving
	  (for any reason)
	* Store pointer to client id locally in struct chmember_t
	* Channel member list is now recreated properly in refresh_nicklist()

2004-02-16 Andrej Kacian <andrej@kacian.sk>
	* Fixed a segfault upon /quit (related to the xml config)
	* Updated AUTHORS file

2004-02-15 Toni Willberg <toniw@iki.fi>
	* Implemented lag detector.

2004-02-11 Andrej Kacian <andrej@kacian.sk>
	* Implemented XML config file instead of jconfig one. The new system
	  still has some bugs, but it's working in general.
	* Fixed bug with unwanted multiple creation of context-nick text tag
	* Updated .c header comments (mostly copyright year)

2004-02-10 Toni Willberg <toniw@iki.fi>
	* Implemented context-sensitive popup-menus for nicknames. Right-click
	  on a nickname to popup a menu.
	* User- and hostnames now shown on join/part/quit/etc...

2004-02-02 Toni Willberg <toniw@iki.fi>
	* to turn on debugging for now on: /set debug on (used to be
	  '/set debug 1')
	* debug boolean now shown also in the preferences GUI

2004-01-25 Ossi Herrala <oherrala@ee.oulu.fi>
	* Do not ask password if user have created keys with no password

2004-01-21 Andrej Kacian <andrej@kacian.sk>
	* nicklists are now sorted alphabetically, tab-completion too

2004-01-09 Andrej Kacian <andrej@kacian.sk>
	* Tab-completion now attempts to complete last word instead of first
	  only

=========================================================================================

2004-01-06 Silky Team
	* Silky 0.4.3 released!

2004-01-05 Andrej Kacian <andrej@kacian.sk>
	* channel/query tabs can now be closed manually after disconnecting
	* all tabs except console are closed when connecting again

2004-01-04 Andrej Kacian <andrej@kacian.sk>
	* channel_remove_by_*() functions now return gracefully if channel
	  struct is not found
	* implemented client-side state monitoring, nearly all events now use it
	* fixed channel struct freeing on disconnect

2004-01-03 Toni Willberg <toniw@iki.fi>
	* fixed crashing bugs related to reconnecting to server and rejoining
	  channels

2004-01-02 Andrej Kacian <andrej@kacian.sk>
	* primitive nickname tab-completion in channels now works
	* fixed a segfaulting bug in command history (win32)

2003-12-27 Andrej Kacian <andrej@kacian.sk>
	* wrote basic support functions for nickname tab-completion in channels

2003-12-24 Andrej Kacian <andrej@kacian.sk>
	* added French translation

2003-12-23 Toni Willberg <toniw@iki.fi>
	* added Hungarian translation

=========================================================================================

2003-12-21 Silky Team
	* Silky 0.4.2 released!

2003-12-20 Andrej Kacian <andrej@kacian.sk>
	* fixed segfault when typing a command after parting a channel
	  (introduced by me, found by me, fixed by me, probably noone ever
	  noticed )
	* fixed a small bug in command parsing

2003-12-20 Toni Willberg <toniw@iki.fi>
	* Moved debug stuff from /set to /config. Removed /set command. Renamed
	  /config command to /set.
	  Now you turn on debugging by "/set debug 1".
	* Changed the RPM package to support Mandrake, RedHat Linux and
	  Fedora Core

2003-12-18 Toni Willberg <toniw@iki.fi>
	* Connect/disconnect menu entries now enabled only when needed. User
	  can not accidentally connect twice now.
	* Automatic scrolling was also fixed today.

2003-12-13 Andrej Kacian <andrej@kacian.sk>
	* fixed OpenBSD compile error (thanks to Joshua Steele)
	* some snprintf->g_snprintf changes (patch by Joshua Steele)

=========================================================================================

2003-12-13 Silky Team
	* Silky 0.4.1 released!

2003-12-13 Andrej Kacian <andrej@kacian.sk>
	* fixed a segfault on sending too long message, limited length to 255

2003-12-13 Toni Willberg <toniw@iki.fi>
	* Changed silc.m4 to support SILC libs and includes to be located in
	  separate dirs
	* Added --with-silc-libs and --with-silc-includes options to
	  configure.in
	* fixed .spec file to support dynamically configured locations for stuff
	* .spec (the rpm package) now requires libsilc (and libsilc-devel for
	  building)

2003-12-12 Patrik Weiskircher <pat@icore.at>
	* merged etc's silc.m4 script

2003-12-11 Andrej Kacian <andrej@kacian.sk>
	* Marked remaining strings for translation
	* Fixed bug with channel topic inputbox focus

2003-11-30 Andrej Kacian <andrej@kacian.sk>
	* Implemented command history. Browse it with up/down arrow.
	* Command history depth can now be set in user preferences.

2003-11-30 Daniel Richards <kyhwana@kyhwana.org
	* Added /msg
	* Added preliminary /config. Uses jconfig stuff. Will only display
	  preferences, will not save changes due to config_save reading from
	  UI settings.

2003-11-28 Daniel Richards <kyhwana@kyhwana.org
	* Added preliminary /query support.

2003-11-27 Daniel Richards <kyhwana@kyhwana.org
	* Fixed actions in query windows

2003-11-25 Andrej Kacian <andrej@kacian.sk>
	* Fixed a bug where connection dialog would show up after user tries
	  to enter command when not connected (pointed out by Daniel Richards)

2003-11-24 Andrej Kacian <andrej@kacian.sk>
	* Added struct for channel data storage (experimental)
	* Got rid of compile-time warnings with -Wall
	* When not connected and user hits ENTER, connection dialog is
	  displayed
	* Enhanced debug output (thanks to Ted Rolle and Patrik Weiskircher)
	* All window titles now display Silky version as well
	* Squished some bugs

2003-11-21 Andrej Kacian <andrej@kacian.sk>

	* Fixed multiple silcclient installation issue in macros/silc.m4
	  (found by Ted Rolle)

2003-11-16 Michele Baldessari <michele@pupazzo.org>

	* Made the message of not finding the silc toolkit a bit clearer

2003-11-13 Andrej Kacian <andrej@kacian.sk>
	* Hopefully fixed inputbox focus issue
	* TAB key no longer takes focus away from inputbox
	* Automated string extraction from glade files (autogen.sh)

2003-11-09 Andrej Kacian <andrej@kacian.sk>
	* Fixed segfault on quitting by closing main window introduced by me.
	* Made strings from all glade files to be extracted for .pot

2003-11-10 Michele Baldessari <michele@pupazzo.org>
	* Removed some autogenerated files (config.h, stamp-h1, stamp-h.in)

2003-11-09 Michele Baldessari <michele@pupazzo.org>
	* Implemented SILC_AUTH_PUBLIC_KEY connection method.

2003-11-09 Andrej Kacian <andrej@kacian.sk>
	* Added i18n support, fixed some small typos in strings.
	* dos2unix-ed everything in src/, no more ^M. :)
	* Support for command abbreviation, will need attention yet.
	* Custom quit messages can now be passed to on_ui_quit() and used with
	  /quit command.

2003-11-09 Daniel Richards <kyhwana@kyhwana.org>
	* Added basic /command support.
	* Changed Passphrase requirements. (no passphrase, or >4 characters)
	* Few other little things re error dialogs.

2003-11-08 Toni Willberg <toniw@iki.fi>
	* Added .cvsignore files, contributed by Michele Baldessari
	* Cleaned the ChangeLog a bit :)

2003-11-04 Toni Willberg <toniw@iki.fi>
	* Preferences are now applied on save, no need to restart anymore.
	* Testing some logos.
	* The fonts are now configurable.
	* Cleaned some code.
	* LIBS, CFLAGS and LDFLAGS from environment works now in configure
	  script.

=========================================================================================

2003-10-31 Toni Willberg <toniw@iki.fi>
	* Released 0.3.1.
	* Win32 build of Silky is now compiled against Dropline GTK+ for
	  Windows 2.2.4.
	* Now using Inno Setup to create a cool installed for Win32.
	* GTK+ libraries are no more included in the Win32 package of Silky.
	  They need to be installed separately. See README.win32.
	* Now Silky figures out the content type of a file the user sends as a
	  binary message. Using mime.types file. The detection is based on
	  extension/mime-type mapping.
	* Implemented WHOIS. Right-mouse-click on a nickname on the nicklist to
	  get a popup menu.
	* Fixed most of the dialogs by adding keyboard shortcuts or actions to
	  work also without mouse.
	* Implemented TOPIC changing.
	* Informational (error, warning, info, feedback) messages are now printed
	  to the active tab instead of the console tab.
	* Removed most of the unnecessary dialogs for error messages; now
	  printing errors as text to the tabs.
	* Now showing the user's nickname always in the GUI.
	* Fixed crashing bugs in MIME messages printing code.
	* Fixed crashing bug in some strcmp calls.
	* Most of the colors are now configurable.
	* Implemented server info showing.
	* Code cleanups; removed some unused blocks of code.

=========================================================================================

2003-10-21 Toni Willberg <toniw@iki.fi>
	* Released 0.3.0

2003-10-18 Toni Willberg <toniw@iki.fi>
	* I feel it's good time to start writing stuff to ChangeLog too...
	* Leaving channel and closing a query tab works now correctly.
	* Color preferences almost fully implemented. Background color is
	  still missing.
	* Silc Toolkit 0.9.10 from CVS (atleast 2003-10-18) is now required
	  for compilation.
	* Silc Server 0.9.14 is now required. Don't try to connect to older
	  version!
