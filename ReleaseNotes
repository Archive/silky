*********************************************************************************
* 2005-03-17 Toni Willberg <toniw@iki.fi> - Release notes for Silky 0.5.4
*********************************************************************************

Silky 0.5.4 has been released!
 * Silky is a secure chat client for GTK+-2, using the SILC protocol.

Getting Silky:
 * http://silky.sourceforge.net/

Reporting bugs:
 * http://bugzilla.gnome.org/

Changes since the previous release:
 * fixes the win32 build
 * fixes runtime warnings

 Read the full changelog from here:
  http://cvs.gnome.org/viewcvs/silky/ChangeLog?view=markup

Authors of this release:
 * Toni Willberg, Andrej Kacian, Juha Räsänen

*********************************************************************************
* 2005-03-17 Toni Willberg <toniw@iki.fi> - Release notes for Silky 0.5.3
*********************************************************************************

Silky 0.5.3 has been released!
 * Silky is a secure chat client for GTK+-2, using the SILC protocol.

Getting Silky:
 * http://silky.sourceforge.net/

Reporting bugs:
 * http://bugzilla.gnome.org/

Changes since the previous release:
 * New keyboard shortcuts: ^j (join channel), ^n (change nickname)
   and ^w (close current tab)
 * New and fixed commands: /CLEAR /LIST /MSG /WHOIS
 * Fixed window placement problem on resize.
 * Implemented the buddy list
 * Improved the lag detector code.
 * New default colors and new color selector UI.
 * Channel message signatures are now verified.
 * Server ports are now configurable.
 * Streamlined the dialogs.
 * User's passphrase can now be changed.
 * New About and Credits -dialogs.
 * Now showing dialog on invite.
 * New dialog for showing user information.

 * New configure options: --with-silc-libs and --with-silc-include
 * Re-written debug functions for developers.
 * Lots of code cleanups and development documentation for developers.
 * CVS and bug tracking has moved from Sourceforge to GNOME.org.

 Read the full changelog from here:
  http://cvs.gnome.org/viewcvs/silky/ChangeLog?view=markup

New translations:
 * Romanian by Cristi Boian
 * Canadian English by Adam Weinberger

Authors of this release:
 * Toni Willberg, Andrej Kacian

*********************************************************************************
* 2004-10-09 Toni Willberg <toniw@iki.fi> - Release notes for Silky 0.5.2
*********************************************************************************

Silky 0.5.2 has been released!

 Silky is a secure chat client for Gtk2.
 http://silky.sourceforge.net/


Getting Silky:
 * Supported packages are available from Debian, Gentoo and Fedora Extras repositories.
 * Source tarball is available from http://sourceforge.net/project/showfiles.php?group_id=86891

 Please report all bugs directly to Silky's bug tracker at our website.
 Also note that we've tested this release only on Linux. The Windows version will
 appear shortly to the website without separate notice.

 Other operating systems are not officially supported, but please let us know about
 possible problems.


Changes briefly since the previous version:

 * better handling of nicklists when disconnecting
 * added Gtk2.2 compatibility
 * added a timeout handling for server connection
 * realname is not set correctly
 * new GETKEY command
 * lots of code cleanup
 * many bugfixes, hopefully all crashers have been fixed
 * server list is now editable
 * nicklist is being automatically sorted
 * fixed text autoscrolling
 * now you can scroll up/down by using PgUp/PgDown keys
 * nicklist background color has been fixed
 * building Silky from sources now requires a SILC Toolkit with pkg-config patch

 Read the full changelog from here:
  http://cvs.sourceforge.net/viewcvs.py/silky/silky/ChangeLog?view=markup



*********************************************************************************
* 2004-04-03 Toni Willberg <toniw@iki.fi> - Release notes for Silky 0.5.1
*********************************************************************************

Silky 0.5.1 is released!

 Silky is a secure chat client for GTK+.

Changes shortly:

 * Tabs can be switched by pressing alt+1..9 and alt+left/right
 * all dialogs now have common layout
 * key generation now has a progress bar
 * preferences system has been rewritten and simplified
 * /set command has been rewritten and is working again
 * config file format has been improved (no more 'type="..."' attribute)
 * "debug" option has been removed from preferences
 * few command-line options have been added to silky, such as debug or version
 * default config file is now installed along with other data files and Silky
   makes use of it, if user's local config file is not found

 * many old bugs fixed and new ones written
 * the code has been cleaned and partially rewritten

 * supports and requires the SILC Toolkit 0.9.12.

 Read the full changelog from here:
  http://cvs.sourceforge.net/viewcvs.py/silky/silky/ChangeLog?view=markup

Getting Silky:

 All releases of Silky are available from:
  http://silky.sourceforge.net/
  http://sourceforge.net/project/showfiles.php?group_id=86891

Authors of this release:
 Toni Willberg, Andrej Kacian, Daniel Richards, Ossi Herrala, Mika Boström


*************************************************************************
* 2004-04-16 Andrej Kacian <andrej@kacian.sk> - Release notes for Silky 0.5.1pre3
*************************************************************************

Silky 0.5.1pre3 is released!

 Silky is an easy to use SILC client for GTK+. Silky is available as
 Linux and Windows binaries as well as a source tarball.

 SILC is Secure Internet Live Conferencing protocol.

Changes shortly:

 * Preferences system has been rewritten and simplified
 * /set command has been rewritten and is working again
 * config file format has been improved (no more 'type="..."' attribute)
 * "debug" option has been removed from preferences
 * few command-line options have been added to silky, such as debug or version
 * default config file is now installed along with other data files and Silky
   makes use of it, if user's local config file is not found

 * many old bugs fixed and new ones written
 * the code has been cleaned and partially rewritten
 * translatable strings have NOT changed since 0.5.1pre2

 Read the full changelog from here:
  http://cvs.sourceforge.net/viewcvs.py/silky/silky/ChangeLog?view=markup

SILC Toolkit version compatibility:

 This version of Silky uses version 0.9.12 of the SILC Toolkit.

Getting Silky:

 All releases of Silky are available from:
  http://silky.sourceforge.net/
  http://sourceforge.net/project/showfiles.php?group_id=86891

Authors of this release:
 Toni Willberg, Andrej Kacian, Daniel Richards, Ossi Herrala, Mika Boström

*************************************************************************
* 2004-04-03 Toni Willberg <toniw@iki.fi> - Release notes for Silky 0.5.1pre1
*************************************************************************

Silky 0.5.1pre1 is released!

 Silky is an easy to use SILC client for GTK+. Silky is available as
 Linux and Windows binaries as well as a source tarball.

 SILC is Secure Internet Live Conferencing protocol.

Changes shortly:

 All dialogs now have common layout, key generation made easier, added
 a progress bar for key generation, added 'Tip of the Day', nicknames are
 now sorted automatically, all messages can now me signed, now requires
 the SILC Toolkit 0.9.12, server keys are now verified on connect,
 disabled binary file sending for now and the channel tab labels now
 inform the user if there's some action in a channel.

 Also many old bugs fixed and new ones written. The code has been
 cleaned and partially rewritten. All the output strings have been
 checked and fixed as needed.

 Read the full changelog from here:
  http://cvs.sourceforge.net/viewcvs.py/silky/silky/ChangeLog?view=markup

SILC Toolkit version compatibility:

 This version of Silky uses version 0.9.12 of the SILC Toolkit.

Getting Silky:

 All releases of Silky are available from:
  http://silky.sourceforge.net/
  http://sourceforge.net/project/showfiles.php?group_id=86891

Authors of this release:
 Toni Willberg, Andrej Kacian, Daniel Richards, Ossi Herrala, Mika Boström

*************************************************************************
* 2004-03-04 Toni Willberg <toniw@iki.fi> - Release notes for Silky 0.5.0
*************************************************************************

Silky 0.5.0 is released!

 Silky is an easy to use SILC client for GTK+. Silky is available as
 Linux and Windows binaries as well as a source tarball. Libraries used
 include gtk2, glib2, libxml2, libglade2 and libsilc.

Changes shortly:

 Fixed tab-completion, sorted nicklists, support for empty password
 for user's key, user's username and hostname are shown on channel
 operations, implemented lag detector, fixed known crash bugs,
 removed [x] buttons and introduced a new one, user is now notified
 when there's action on channel, configuration file format changed to xml,
 incoming invites are now shown, implemented settings tab for channels,
 added Afrikaans translation.

 Read the full changelog from here:
  http://cvs.sourceforge.net/viewcvs.py/silky/silky/ChangeLog?view=markup

Upgrade information:

 If you are upgrading from older version of Silky, you need to remove your
 old silky.conf file, because the format has changed.

SILC Toolkit version compatibility:

 This version of Silky requires version 0.9.11 of the SILC Toolkit. Version
 0.9.12 does not work. The next release of Silky will use 0.9.12.

Getting Silky:

 All releases of Silky are available from:
  http://silky.sourceforge.net/
  http://sourceforge.net/project/showfiles.php?group_id=86891



*************************************************************************
* 2004-01-06 Toni Willberg <toniw@iki.fi> - Release notes for Silky 0.4.3
*************************************************************************

Silky is an easy to use SILC client for GTK+. Silky is available as
Linux and Windows binaries as well as a source tarball.

Release 0.4.3 brings you great amount of bugfixes, new features and
new translations.

Smashed bugs include the server reconnection crash, the command history
crash and couple of other kind of crashers.

New features include nickname tab-completion. New languages are Hungarian
and French.

Known issues: Silky doesn't work in Windows 98. Translations do not work
in any Windows version at the moment.

The Silky project now uses sf.net's bug tracker. Please report all bugs
you find!
