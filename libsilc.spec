Summary: SILC Client Library
Name: libsilc
Version: 0.9.12
Release: 0.fdr.6
License: GPL
Group: System Environment/Libraries
URL: http://www.silcnet.org/
Source0: silc-toolkit-%{version}.tar.gz
Patch: silc-toolkit-0.9.12-tutorial.patch
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Epoch: 0
BuildRequires: libtool

%description
SILC Client Library libraries for SILC clients.

%package devel
Summary: Headers and shared libraries for %{name}
Group: Development/Libraries
Requires: libsilc = %{epoch}:%{version}-%{release}
Requires: pkgconfig
%description devel
The SILC Toolkit development libraries and headers. Required for building SILC clients.

%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: libsilc = %{epoch}:%{version}-%{release}
%description doc
The SILC Toolkit documentation in HTML format. Useful for writing new SILC applications.

%prep
%setup -q -n silc-toolkit-%{version}
%patch -p1

%build
%configure --without-irssi --without-silcd --libdir=%{_libdir} --enable-shared \
           --includedir=%{_includedir}/silc --with-simdir=%{_libdir}/silc/modules \
           --with-docdir=%{_docdir}/%{name}-%{version}

make

%install
# clear the buildroot
rm -rf $RPM_BUILD_ROOT

# make install
make DESTDIR=$RPM_BUILD_ROOT install
chmod 0755 ${RPM_BUILD_ROOT}%{_libdir}/lib* ${RPM_BUILD_ROOT}%{_libdir}/silc/modules/*.so

# remove files we don't want into the package, but are being installed to buildroot
rm -rf $RPM_BUILD_ROOT%{_sysconfdir}/silcalgs.conf               $RPM_BUILD_ROOT%{_sysconfdir}/silcd.conf \
 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/INSTALL            $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/README \
 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/README.MACOSX      $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/README.WIN32  \
 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/TODO               $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/example_silc.conf  \
 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/example_silcd.conf $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/examples/ \
 $RPM_BUILD_ROOT%{_mandir}                        $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/CHANGES \
 $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}/tutorial

mkdir -p $RPM_BUILD_ROOT%{_libdir}/pkgconfig
cat << EOF > $RPM_BUILD_ROOT%{_libdir}/pkgconfig/silc.pc
prefix=%{_prefix}
exec_prefix=\${prefix}
libdir=%{_libdir}
includedir=%{_includedir}
 
Name: silc
Version: 0.9.12
Description: SILC library.
Libs: -L\${libdir} -lsilcclient -lsilc
Cflags: -I\${includedir}/silc
EOF

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%clean
rm -rf $RPM_BUILD_ROOT

# the main package libsilc
%files
%defattr(0755, root, root, 0755)
%{_libdir}/libsilc-1.0.so.2
%{_libdir}/libsilc-1.0.so.2.1.0
%{_libdir}/libsilcclient-1.0.so.2
%{_libdir}/libsilcclient-1.0.so.2.1.0
%dir %_libdir/silc
%dir %_libdir/silc/modules
%{_libdir}/silc/modules/*.so
%defattr(0644, root, root, 0755)
%dir %_docdir/%{name}-%{version}
%{_docdir}/%{name}-%{version}/COPYING
%{_docdir}/%{name}-%{version}/FAQ
%{_docdir}/%{name}-%{version}/CREDITS

# sub-package libsilc-devel
%files devel
%defattr(0644, root, root, 0755)
%{_libdir}/libsilc.so
%{_libdir}/libsilc.a
%{_libdir}/libsilc.la
%{_libdir}/libsilcclient.so
%{_libdir}/libsilcclient.a
%{_libdir}/libsilcclient.la
%{_libdir}/pkgconfig/silc.pc
%dir %_includedir/silc
%{_includedir}/silc/*.h

# sub-package libsilc-doc
%files doc
%defattr(0644, root, root, 0755)
%{_docdir}/%{name}-%{version}/README.CVS
%{_docdir}/%{name}-%{version}/CodingStyle
%{_docdir}/%{name}-%{version}/draft-*.txt
%{_docdir}/%{name}-%{version}/toolkit/

%changelog
* Tue Sep 1 2004 Toni Willberg <toniw@iki.fi>
- 0.9.12-0.fdr.6 - Had to remove smp_mflags because build fails with them (Michael Schwendt)
* Tue Aug 31 2004 Toni Willberg <toniw@iki.fi>
- 0.9.12-0.fdr.5 - corrections to lib and include path (from Michael Schwendt)
* Tue Aug 31 2004 Toni Willberg <toniw@iki.fi>
- 0.9.12-0.fdr.4 - post/postun /sbin/ldconfig
  (Patch 823 from Stu Tomlinson)
* Tue Aug 31 2004 Toni Willberg <toniw@iki.fi>
- 0.9.12-0.fdr.3 - Move libs to %{_libdir} and add a silc.pc 
  (Patch 815 from Stu Tomlinson)
* Tue Aug 17 2004 Toni Willberg <toniw@iki.fi>
- fix so permissions and hardcoded paths (patch from Michael Schwendt)
* Mon Jul 5 2004 Toni Willberg <toniw@iki.fi>
- Fixed various errors
* Sun Jul 4 2004 Toni Willberg <toniw@iki.fi>
- Initial version for Fedora
