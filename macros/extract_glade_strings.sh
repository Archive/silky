#!/bin/sh

if test -z `which intltool-extract 2>/dev/null`; then
 echo Intltool not found, skipping translatable string extraction from glade files...
 echo WARNING: .pot template file will not be complete! Install intltool.
 exit 1;
else
 for file in `ls *.glade`; do
  echo Extracting translatable strings from glade files...
  intltool-extract --type=gettext/glade $file;
 done;
 mv *.glade.h po;
fi;
