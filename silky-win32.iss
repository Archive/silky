;
; silky-win32.iss - The config file for Inno Setup.
; http://silky.sourceforge.net/
;

[Setup]
AppName=Silky
AppVerName=Silky 0.5.4
AppVersion=0.5.4
AppPublisher=Toni Willberg
AppPublisherURL=http://silky.sourceforge.net/
AppCopyright=Copyright (C) 2004 Toni Willberg
AppSupportURL=http://silky.sourceforge.net/
AppUpdatesURL=http://silky.sourceforge.net/
DefaultDirName={pf}\Silky
DefaultGroupName=SILC
DisableStartupPrompt=yes
WindowShowCaption=yes
WindowVisible=no
LicenseFile=COPYING
Compression=bzip/9
SourceDir=z:/cross/silky-win32-dist
OutputDir=z:/cross/silky-win32-dist
OutputBaseFilename=silky-0.5.4-win32
ChangesAssociations=no

[Tasks]
Name: "desktopicon"; Description: "Create a &desktop icon"; GroupDescription: "Additional icons:"

[Registry]
; Add Gtk+ libs to the path
Root: HKLM; Subkey: "Software\Microsoft\Windows\CurrentVersion\App Paths\silky.exe"; Flags: uninsdeletekeyifempty
Root: HKLM; Subkey: "Software\Microsoft\Windows\CurrentVersion\App Paths\silky.exe"; ValueType: string; ValueData: "{app}\silky.exe"; Flags: uninsdeletevalue
Root: HKLM; Subkey: "Software\Microsoft\Windows\CurrentVersion\App Paths\silky.exe"; ValueType: string; ValueName: "Path"; ValueData: "{app};{code:GetGtkPath}\lib"; Flags: uninsdeletevalue
Root: HKCU; Subkey: "Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers"; ValueType: String; ValueName: "{app}\silky.exe"; ValueData: "WIN2000"; Flags: Uninsdeletekey

[Files]
Source: "silky.exe"; DestDir: "{app}";
Source: "*.glade"; DestDir: "{app}";
Source: "COPYING"; DestDir: "{app}"; DestName: "COPYING.txt";
Source: "mime.types"; DestDir: "{app}";
Source: "README"; DestDir: "{app}"; DestName: "README.txt";
Source: "README.win32"; DestDir: "{app}"; DestName: "README.win32.txt";
Source: "ChangeLog"; DestDir: "{app}"; DestName: "ChangeLog.txt";
Source: "ReleaseNotes"; DestDir: "{app}"; DestName: "ReleaseNotes.txt";


[Icons]
Name: "{group}\Silky"; Filename: "{app}\silky.exe"; Comment: "Silky is an easy to use SILC client"; WorkingDir: "{app}";
Name: "{group}\Silky's website"; Filename: "http://silky.sourceforge.net/";
Name: "{group}\Uninstall Silky"; Filename: "{uninstallexe}";
Name: "{userdesktop}\Silky"; Filename: "{app}\silky.exe"; Tasks: desktopicon; Comment: "Silky"; WorkingDir: "{app}";

[Code]

var
  Exists: Boolean;
  GtkPath: String;

function GetGtkInstalled (): Boolean;
begin
  Exists := RegQueryStringValue (HKLM, 'Software\GTK\2.0', 'Path', GtkPath);
  if not Exists then begin
    Exists := RegQueryStringValue (HKCU, 'Software\GTK\2.0', 'Path', GtkPath);
  end;
   Result := Exists
end;

function GetGtkPath (S: String): String;
begin
    Result := GtkPath;
end;

function InitializeSetup(): Boolean;
begin
  Result := GetGtkInstalled ();
  if not Result then begin
    MsgBox ('Please install the GTK+ Runtime Environment before installing Silky.  You can obtain GTK+ from http://www.dropline.net/gtk/', mbError, MB_OK);
  end;
end;
