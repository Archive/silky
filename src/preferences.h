#ifndef PREFERENCES_H__
#define PREFERENCES_H__

gboolean prefs_load_config();
void prefs_populate_gui();
void prefs_save_gui();
void prefs_show_to_console();

gchar *prefs_get(gchar *name);
gboolean prefs_set(gchar *name, gchar *value);
gint prefs_widget_type(gchar *name);

gboolean prefs_str_to_bool(gchar *val);
gchar *prefs_bool_to_str(gboolean val);

typedef enum
{
 WIDGET_ENTRY,
 WIDGET_CHECKBUTTON,
 WIDGET_COLORBUTTON,
 WIDGET_FONTBUTTON,
 WIDGET_SPINBUTTON,
} PrefWidgetTypes;

#define PREF_ON		"ON"
#define PREF_OFF	"OFF"

#endif
