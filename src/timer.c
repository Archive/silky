/*
    Silky - A GTK+ client for SILC.
    Copyright (C) 2003,2004 Toni Willberg

    - Functions used by timers

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    http://silky.sourceforge.net/

*/

#include <glade/glade.h>
#include <gtk/gtk.h>
#include <silcincludes.h>
#include <silcclient.h>

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "timer.h"
#include "support.h"
#include "log.h"
#include "common.h"

gint lastpong; /* last time we received a ping */

extern silkyStruct *silky;
extern GladeXML *xmlmain;

/* PING
   This gets called periodically by GTK
  Shows lag time in the UI if it's over N seconds
*/

gboolean silky_ping(gpointer data) {
  GtkWidget *statusbar;
  gint pong = 0;
  guint context = 0;

  if (CONNECTED && silky && silky->client) { /* do nothing if we are not initialized and connected */

    if(!lastpong)
      lastpong = time(NULL);

    pong = time(NULL) - lastpong;

    statusbar = glade_xml_get_widget (xmlmain, "silkystatusbar");
    context = gtk_statusbar_get_context_id(GTK_STATUSBAR(glade_xml_get_widget (xmlmain, "silkystatusbar")), "Lag");

    if (pong > 15) {
      debug ("No network activity in %d seconds, sending PING to server %s", pong, silky->conn->local_entry->server );
    
      /* send PING to the server */
      silc_client_command_call(silky->client, silky->conn, NULL, "PING", silky->conn->local_entry->server,
			       NULL);
      gtk_widget_show(statusbar);
    }

    if (pong > 30) { /* show lag timer if lag is more than N seconds */
      /* I18N lag */
      debug("Showing LAG'O'METER for user");
      gtk_statusbar_push(GTK_STATUSBAR(statusbar), context, g_strdup_printf(ngettext("Network problem: Server did not respond in %d second, please wait...", "Network problem: Server did not respond in %d seconds, please wait...", pong), pong));
      gtk_widget_show(statusbar);

    }
    else {
      gtk_statusbar_push(GTK_STATUSBAR(statusbar), context, "");
      gtk_widget_hide(statusbar);
    }

  }

  return TRUE;
}


/*
  a small function called from timer, to update a progressbar
*/
gboolean silky_progressbar_update(GtkProgressBar *progressbar) {
  if (GTK_IS_PROGRESS_BAR(progressbar)) {
    gtk_progress_bar_pulse(progressbar);
    return TRUE;
  }
  else {
    return FALSE; /*
		     This happens if user closes the progress dialog while were are here.
		  */
  }

}
