#ifndef UI_BUDDYLIST_H__
#define UI_BUDDYLIST_H__

#include <gtk/gtk.h>
#include "buddylist.h"

typedef enum {
  BUDDYLIST_COL_NAME = 0,
  BUDDYLIST_COL_BUDDYPTR = 1,
  BUDDYLIST_COL_ICON = 2,
  BUDDYLIST_COL_BOLD = 3,
  BUDDYLIST_COL_TYPE = 4,
} SilkyBuddyListColumns;

typedef enum {
	BUDDYLIST_TYPE_GROUP,
	BUDDYLIST_TYPE_BUDDY,
	BUDDYLIST_TYPE_ATTRIBUTE
} SilkyBuddyListTypes;

void gui_set_up_buddylist_treeview();
void refresh_gui_buddylist();
gboolean on_command_buddy_add (GtkMenuItem*, gpointer);
gboolean ui_buddy_remove (GtkWidget *wid, gpointer fingerprint);
gboolean buddy_selected (GtkTreeView*, GdkEventButton*);
gboolean ui_buddy_update_foreachfunc (GtkTreeModel *model, 	 
                                          GtkTreePath *path, 	 
                                          GtkTreeIter *iter, 	 
                                          gpointer data);
gboolean ui_buddy_update_by_fp(gchar *str_fingerprint);
gboolean ui_buddy_update (SilkyBuddy *b);

gint buddylist_iter_get_type(GtkTreeModel *model, GtkTreeIter iter);

#endif
