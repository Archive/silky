#ifndef CHANNEL_H__
#define CHANNEL_H__


#include <silcincludes.h>
#include <silcclient.h>
#include <glade/glade.h>

#define CHANNEL_NAME(ch)	((ch)->channel_entry->channel_name)
#define CHANNEL_TOPIC(ch)	((ch)->channel_topic)
#define CHANNEL_CMODE(ch)	((ch)->channel_entry->mode)

#define CHMEMBER_NAME(chm)	((chm)->client->nickname)

#define IS_CHANNEL_FOUNDER(chm) ((chm)->chuser->mode & SILC_CHANNEL_UMODE_CHANFO)
#define IS_CHANNEL_MUTED(chm)	((chm)->chuser->mode & SILC_CHANNEL_UMODE_QUIET)
#define IS_CHANNEL_OP(chm)	((chm)->chuser->mode & SILC_CHANNEL_UMODE_CHANOP)

#define I_AM_OP(ch) \
    (((get_channel_user_by_entry(ch, silky->conn->local_entry))->mode & SILC_CHANNEL_UMODE_CHANOP) ? TRUE : FALSE)
#define I_AM_FOUNDER(ch) \
    (((get_channel_user_by_entry(ch, silky->conn->local_entry))->mode & SILC_CHANNEL_UMODE_CHANFO) ? TRUE : FALSE)
#define I_AM_QUIET(ch) \
    (((get_channel_user_by_entry(ch, silky->conn->local_entry))->mode & SILC_CHANNEL_UMODE_QUIET) ? TRUE : FALSE)

/* CHANNEL STRUCT */
/* FIXME: any other elements needed? */
struct channel_t {
  struct channel_t	*next;

  SilcChannelEntry	channel_entry;
  gchar			topic[257]; /* channel topic */

  struct chmember_t	*members; /* pointer to linked list of members */

  struct chmember_t	*tab_member; /* used with tab-completion */
  gchar			*tab_prefix; /* tab-completion prefix */
  gchar			*tab_text;   /* text (if any) before word being completed */

  GladeXML		*glade_xml;
  gint pagenr; /* which GtkNotebook page (of index) contains this channel */
  gpointer *label_text; /* label title */
  gpointer *label_image; /* label image */
};

typedef struct channel_t SilkyChannel;


/* CHANNEL MEMBER STRUCT */
/* used for tab-completion */
struct chmember_t {
    struct chmember_t	*next;
    SilcChannelUser	chuser;
    SilcClientEntry	client;		/* local persistent copy */
};

typedef struct chmember_t SilkyChannelMember;

SilkyChannel *channel_find_by_name( gchar *name );
SilkyChannel *channel_find_by_pagenr( gint pagenr );
SilkyChannel *channel_find_by_entry( SilcChannelEntry entry );

SilkyChannel *channel_add( SilcChannelEntry entry );

gboolean channel_remove_by_name( gchar *name );
gboolean channel_remove_by_entry( SilcChannelEntry entry );

void channel_topic_set_by_name( gchar *name, gchar *topic );
void channel_topic_set_by_entry( SilcChannelEntry entry, gchar *topic );

void channel_pagenr_set_by_name( gchar *name, gint pagenr );
void channel_pagenr_set_by_entry( SilcChannelEntry entry, gint pagenr );

void channel_mode_set_by_name( gchar *name, SilcUInt32 cmode );
void channel_mode_set_by_entry( SilcChannelEntry entry, SilcUInt32 cmode );



struct chmember_t *channel_member_find_by_nick( SilkyChannel *channel, gchar *name );
SilkyChannelMember *channel_member_find_by_entry( SilkyChannel *channel, SilcClientEntry client );
SilkyChannelMember *channel_member_add( SilkyChannel *channel, SilcChannelUser chuser );

gint channel_member_remove_by_entry( SilkyChannel *channel, SilcClientEntry client) ;

void channel_member_change_client_entry( SilkyChannel *channel, SilcClientEntry oldclient, SilcClientEntry newclient);

void channel_free_all( void );
void channel_member_free_all( SilkyChannel *channel );

void update_channel_nicklist (SilcClientEntry old_entry, SilcClientID *old_client_id, SilcClientEntry new_client_entry);
void refresh_nicklist (SilcChannelEntry channel_entry);
void refresh_nicklist_resolved(SilcClient client,
			       SilcClientConnection conn,
			       SilcClientEntry *clients,
			       SilcUInt32 clients_count,
			       void *context);
void channels_nick_change(SilcClientEntry old_entry, SilcClientID *old_client_id, SilcClientEntry new_client_entry);

#define I_AM_CHANOP(ch)	\
    (((get_channel_user_by_entry(ch, silky->client))->mode & SILC_CHANNEL_UMODE_CHANOP) ? TRUE : FALSE)

void silky_join_channel(const gchar *channel_name);
void silky_leave_channel(const gchar *channel_name);

#endif
