#ifndef SERVERS_H__
#define SERVERS_H__


struct server_t {
  struct server_t 	*next;
  gchar			*hostname;	/* the hostname of a server */
  gint			port;		/* tcp port */
  gboolean		autoconnect;	/* guess what? */
  gchar			*passphrase;	/* server passphrase */
};

typedef struct server_t SilkyServer;

typedef struct {
  gboolean ready;
  gint status;
  gchar *hostname;
  gint port;
} NewServerConnection;

gboolean init_servers(gchar *serverspath);
gboolean parse_servers();

SilkyServer *server_find_by_hostport(gchar *host, gint port);
SilkyServer *server_add(gchar *host, gint port);

gboolean server_set_passphrase(SilkyServer *server, const gchar *passphrase);

gboolean server_remove( SilkyServer *server );

gboolean servers_store_to_xml();
void silky_connect_server_threaded(NewServerConnection*);
gboolean silky_connect_server_idle(NewServerConnection*);


#endif
