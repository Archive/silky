#ifndef XMLLAYOUT_H__
#define XMLLAYOUT_H__

#include <glib.h>
#include <libxml/parser.h>

typedef struct {
  xmlChar	*name;
  gint 		parent;
  gint		attributes;
  gchar 	*default_value;
} SilkyXMLElement;

typedef struct {
  gint  attribute;
  gchar *name;
  gchar *default_value;
} SilkyXMLAttribute;

typedef enum {
    CONFIG_MAIN,	/* main config file */
    CONFIG_SERVERS,	/* favorite/recent servers */
    CONFIG_BUDDIES,	/* buddylist stored here */
    NUM_CONFIG		/* total number of config entities */
} SilkyConfigTypes;

typedef enum {
  ATTRN_VERSION,
  ATTRN_CONFIG_VERSION,
  NUM_ATTRN		/* total number of attributes */
} SilkyXMLAttributes;

typedef enum {
  ATTR_VERSION = 1<<1,
  ATTR_CONFIG_VERSION = 1<<2
} SilkyXMLAttributeFlags;

/************************* MAIN */
/* keep this in same order as xml_elements[0] ! */
typedef enum {
    CM_NULL,
    CM_ROOT,

    CM_USERINFO,
    CM_USERINFO_NICKNAME,
    CM_USERINFO_REALNAME,
    CM_USERINFO_QUITMESSAGE,

    CM_GUI,
    CM_GUI_COLORS,
    CM_GUI_COLORS_MYNICKNAME,
    CM_GUI_COLORS_MYTEXT,
    CM_GUI_COLORS_MYACTION,
    CM_GUI_COLORS_NICKNAMES,
    CM_GUI_COLORS_TEXT,
    CM_GUI_COLORS_ACTIONS,
    CM_GUI_COLORS_TIMESTAMP,
    CM_GUI_COLORS_SERVERMESSAGES,
    CM_GUI_COLORS_INPUTBOX,
    CM_GUI_COLORS_BACKGROUND,
    CM_GUI_COLORS_HIGHLIGHTWORDS,
    CM_GUI_COLORS_HIGHLIGHTMYNICK,

    CM_GUI_HIGHLIGHTS,
    CM_GUI_HIGHLIGHTS_OWNNICK,
    CM_GUI_HIGHLIGHTS_WORD,

    CM_GUI_FONT,
    CM_GUI_DEBUG_USEC,
    CM_GUI_CMDHISTORYDEPTH,
    CM_GUI_TIPS,

    CM_SECURITY,
    CM_SECURITY_MESSAGE_SIGNING,
    CM_SECURITY_SIGN_CHANNEL_MESSAGES,
    CM_SECURITY_SIGN_PRIVATE_MESSAGES,
    CM_SECURITY_SIGN_CHANNEL_ACTIONS,
    CM_SECURITY_SIGN_PRIVATE_ACTIONS,

    NUM_CM		/* total number of elements */
} SilkyXMLMainElements;

/************************* SERVERS */
/* keep this in same order as xml_elements[1] ! */
typedef enum {
    CS_NULL,
    CS_ROOT,

    CS_SERVER,
    CS_SERVER_HOSTNAME,
    CS_SERVER_PORT,
    CS_SERVER_AUTOCONNECT,
    CS_SERVER_PASSPHRASE,

    NUM_CS
} SilkyXMLServerElements;

typedef enum {
    CB_NULL,
    CB_ROOT,

    CB_BUDDY,
    CB_BUDDY_NICKNAME,
    CB_BUDDY_REALNAME,
    CB_BUDDY_FINGERPRINT,
    CB_BUDDY_KEYVERIFIED,
    CB_BUDDY_NOTES,

    CB_BUDDY_ATTR,
    CB_BUDDY_ATTR_LANGUAGES,
    CB_BUDDY_ATTR_PREFCONTACT,
    CB_BUDDY_ATTR_TIMEZONE,
    CB_BUDDY_ATTR_GEOLOCATION,

    NUM_CB
} SilkyXMLBuddyElements;


#endif
