#ifndef TIPS_H__
#define TIPS_H__

#define TIPS_FILE	GLADEDIR"tips.txt"

#define FALLBACK_TIP	N_("Bug your system administrator to install some tips.")
#define MAX_TIPS 50

gchar *silky_get_random_tip();

#endif
