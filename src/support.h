#ifndef SUPPORT_H__
#define SUPPORT_H__

#include <gtk/gtk.h>

#include <silcclient.h>
#include <silcincludes.h>
#include "channel.h"
#include "query.h"


/*----------------------- support.h --------------------------------*/

/* our client */
typedef struct {
    SilcClient client;		/* The actual SILC Client */
    SilcClientConnection conn;	/* Connection to the server */
    int state;			/* Client's connection state */
    SilcRng rng;    /* Cryptographically strong RNG */
} silkyStruct;
/* FIXME: move silkyStruct to some other .h */

/* this stores some info about resolved names in the channel */
typedef struct {
    SilcChannelEntry channel;
    bool retry;
} SilcJoinResolve;

/* Ted's - Daniel */
#define countof(a) (sizeof(a) / sizeof(a[0]))
gchar * utf_timestamp (void);

void add_user_to_nicklist (GtkListStore *liststore, SilcChannelEntry channel, SilcChannelUser user);

void mime_types_list_destroy (void);
gchar *mime_types_get_type(gchar *filename);
GHashTable *mime_init(gchar *file);


gboolean context_nick_event(GtkTextTag *texttag, GObject *arg1, GdkEvent *event, GtkTextIter *arg2, gpointer user_data);

SilcChannelUser get_channel_user_by_entry(SilcChannelEntry channel, SilcClientEntry client);
gboolean silky_read_mimetable();

gboolean silky_create_serverkeydir(gchar *serverkeydir);
gboolean silky_create_silkydir(gchar *silkydir);
gchar *silky_get_silkydir(gchar *homedir);
gchar *silky_get_homedir();

int debug_value;
int usec_value;

/* states for silkyStruct->state */
enum {
    STATE_DISCONNECTED,
    STATE_CONNECTING,
    STATE_CONNECTED,
    STATE_DISCONNECTING,
    NUM_STATES
};

#define DISCONNECTED	(((silky->state == STATE_DISCONNECTED) && (!silky->conn)) ? 1 : 0)
#define CONNECTING	(((silky->state == STATE_CONNECTING) && (!silky->conn)) ? 1: 0)
#define CONNECTED	(((silky->state == STATE_CONNECTED) && (silky->conn)) ? 1 : 0)
#define DISCONNECTING	(((silky->state == STATE_DISCONNECTING) && (!silky->conn)) ? 1: 0)

void silky_set_state(int state);

void hilight_channel_window_by_ch(SilkyChannel *ch, gint type);
void hilight_query_window_by_q(SilkyQuery *q, gint type);

enum {
    HILIGHT_MOTION,	/* other users join, part, kick, quit, ... */
    HILIGHT_SPEECH,	/* public action or public message */
    HILIGHT_WORD,	/* word from highlight word list */
    HILIGHT_NICK,	/* my nick was mentioned */
};


void silky_nickname_parse(const char *nickname, char **ret_nickname);

unsigned int get_random_number(const unsigned int min, const unsigned int max);

void * silky_get_active_textview ();
int verify_message_signature(SilcClientEntry sender, SilcMessageSignedPayload sig, SilcMessagePayload payload);

/*--> End of support.h */

#endif
