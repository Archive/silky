/*
    Silky - A GTK+ client for SILC.
    Copyright (C) 2003, 2004, 2005 Toni Willberg

    - "Tip of the day"-related functions

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    http://silky.sourceforge.net/

*/
#include <glib.h>
#include <gtk/gtk.h>
#include <glib/gstdio.h>
#include <glib/gprintf.h>

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "tips.h"
#include "log.h"
#include "support.h" /* get_random_number() */

#include "common.h"


gchar *silky_get_random_tip() {
  gchar *tips_path = NULL, *tips_str = NULL, **tips = NULL, *tip = NULL;
  gint num, i;
  GError *error = NULL;

  debug("showing a tip");

  tips_path = g_strdup_printf("%s/%s", GLADEDIR, "tips.txt");

  if( g_file_test(tips_path, G_FILE_TEST_EXISTS) ) {
    if( !g_file_test(tips_path, G_FILE_TEST_IS_REGULAR) ) {
      debug("Can not open config file, it is not a regular file! Using FALLBACK_TIP");
      g_free(tips_path);
      return g_strdup(FALLBACK_TIP);
    }
  } else {
    debug("File %s doesn not exist, using FALLBACK_TIP", tips_path);
    g_free(tips_path);
    return g_strdup(FALLBACK_TIP);
  }

  g_file_get_contents(tips_path, &tips_str, NULL, &error);
  g_free(tips_path);

  if( tips_str ) {
    tips = g_strsplit(tips_str, "\n", MAX_TIPS);
  } else {
    debug("Tips file empty, using FALLBACK_TIP");
    g_free(tips_str);
    g_free(tips);
    return g_strdup(FALLBACK_TIP);
  }

  for( num = 0; tips[num] && g_utf8_strlen(tips[num], -1); num++ ) {}

  i = get_random_number(0, num);

  /* one more sanity check */
  if( tips[i] && g_utf8_strlen(tips[i], -1) ) {
    debug("Returning tip '%s'", tips[i]);
    tip = g_strdup(tips[i]);
    g_free(tips_str);
    g_free(tips);
    return tip;
  }

  g_free(tips_str);
  g_free(tips);
  return g_strdup(FALLBACK_TIP);
}
