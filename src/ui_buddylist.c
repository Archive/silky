#include <gtk/gtk.h>
#include <glade/glade.h>

#include "silcincludes.h"
#include "silcclient.h"

#include "buddylist.h"
#include "ui_buddylist.h"
#include "log.h"
#include "support.h" /* for CONNECTED/DISCONNECTED */
#include "config.h"
#include "callbacks.h"

#include "common.h"
#include "keys.h" /* gen_keyfile_path() */

extern GladeXML *xmlmain;
extern SilkyBuddy *buddies; /* from buddylist.c */
extern silkyStruct *silky; /* from main.c */

/**
 * gui_set_up_buddylist_treeview
 *
 * This creates the treeview for the Buddy List window.
 *
 * @Returns: nothing
 **/
void gui_set_up_buddylist_treeview() {
  GtkWidget *treeview;
  GtkCellRenderer *renderer_text;
  GtkCellRenderer *renderer_pixbuf;
  GtkTreeStore *treestore;
  GtkTreeViewColumn *column;

  debug_info("creating buddy list treeview");

  /* create store */
  treestore = gtk_tree_store_new (
				  5,
				  G_TYPE_STRING,  /* text column */
				  G_TYPE_POINTER, /* silkybuddy pointer */
				  G_TYPE_STRING,  /* icon stock-id */
				  G_TYPE_INT,	  /* bold */
				  G_TYPE_INT	  /* item type */
                          );
  treeview = glade_xml_get_widget(xmlmain, "buddylist_treeview");
  if (!treeview) {
    debug("can't find the treeview");
    return;
  }


  /* set the model for treeview, overwrites the old one if present */
  gtk_tree_view_set_model (GTK_TREE_VIEW(treeview), GTK_TREE_MODEL(treestore) );

  renderer_text = gtk_cell_renderer_text_new();
  renderer_pixbuf = gtk_cell_renderer_pixbuf_new();

  /* status info */
  column = gtk_tree_view_column_new();
  gtk_tree_view_column_pack_start(column, renderer_pixbuf, FALSE);
  gtk_tree_view_column_add_attribute(column, renderer_pixbuf, "stock-id", BUDDYLIST_COL_ICON);

  /* nickname */
  gtk_tree_view_column_pack_start(column, renderer_text, FALSE);
  gtk_tree_view_column_add_attribute(column, renderer_text, "text", BUDDYLIST_COL_NAME);
  gtk_tree_view_column_add_attribute(column, renderer_text, "weight", BUDDYLIST_COL_BOLD);
  gtk_tree_view_column_set_title(column, _("Buddy List") );
  gtk_tree_view_append_column (GTK_TREE_VIEW(treeview), column);

  debug("created");

}




/**
 * refresh_gui_buddylist:
 *
 * This refreshes the Buddy List window.
 *
 * @Returns: nothing
 **/
void refresh_gui_buddylist() {
  GtkTreeStore *treestore;
  SilkyBuddy *b;
  GtkTreeIter online_groupiter, offline_groupiter, childiter;
  GtkTreeView *treeview;

  gchar *online_group = (_("Online buddies"));
  gchar *offline_group = (_("Offline buddies"));

  debug_info("refreshing the buddy list UI");

  
  treeview = GTK_TREE_VIEW(glade_xml_get_widget(xmlmain, "buddylist_treeview"));
  treestore = GTK_TREE_STORE(gtk_tree_view_get_model(treeview));

 /* clear the list */
 gtk_tree_store_clear(treestore);

 /* ONLINE group */
 gtk_tree_store_append(treestore, &online_groupiter, NULL);
 gtk_tree_store_set(treestore, &online_groupiter, BUDDYLIST_COL_NAME, online_group, BUDDYLIST_COL_BOLD, PANGO_WEIGHT_ULTRABOLD, BUDDYLIST_COL_TYPE, BUDDYLIST_TYPE_GROUP, -1);
 

 /* OFFLINE group */
 gtk_tree_store_append(treestore, &offline_groupiter, NULL);
 gtk_tree_store_set(treestore, &offline_groupiter, BUDDYLIST_COL_NAME, offline_group, -1);
 gtk_tree_store_set(treestore, &offline_groupiter, BUDDYLIST_COL_NAME, offline_group, BUDDYLIST_COL_BOLD, PANGO_WEIGHT_ULTRABOLD, BUDDYLIST_COL_TYPE, BUDDYLIST_TYPE_GROUP, -1);


 /* append all BUDDIES of GROUP */
 for( b = buddies; b; b = b->next ) {
   debug("setting icon for '%s' (%s)", b->nickname, b->fingerprint);

   /* ICON column */
   if (!CONNECTED) {

     gtk_tree_store_append(treestore, &childiter, &offline_groupiter); /* new line */
     debug("set unknown icon as we are not connected");
     gtk_tree_store_set(treestore, &childiter, BUDDYLIST_COL_ICON, GTK_STOCK_DIALOG_QUESTION, -1); /* status icon */

   } else {

     gtk_tree_store_append(treestore, &childiter, &online_groupiter); /* new line */
     switch (b->mode) {

     case SILC_UMODE_DETACHED:
       gtk_tree_store_set(treestore, &childiter, BUDDYLIST_COL_ICON, GTK_STOCK_DIALOG_WARNING, -1); /* column 0, status image */
       break;

     case SILC_UMODE_GONE:
       gtk_tree_store_set(treestore, &childiter, BUDDYLIST_COL_ICON, GTK_STOCK_DIALOG_INFO, -1); /* column 0, status image */
       break;

     default:
       /* all other modes use the network icon */
       gtk_tree_store_set(treestore, &childiter, BUDDYLIST_COL_ICON, GTK_STOCK_JUMP_TO, -1); /* column 0, status image */
       break;
     }
   }

   /* nickname, buddy pointer, item type */
   gtk_tree_store_set(treestore, &childiter, BUDDYLIST_COL_NAME, g_strdup(b->nickname), BUDDYLIST_COL_BUDDYPTR, b, BUDDYLIST_COL_TYPE, BUDDYLIST_TYPE_BUDDY, -1);

   /* this is done manually here as GTK doesn't do it automatically */
   gtk_tree_view_expand_all(treeview);

 }


 debug("ready");
}


/*
  Called as a UI callback when user wants to remove a buddy.
  Asks the buddy to be removed, and updates the UI.
*/
gboolean ui_buddy_remove (GtkWidget *wid, gpointer fingerprint) {
  debug("asking to remove a buddy with fingerprint '%s'", fingerprint);
  buddy_remove_by_fp(fingerprint);

  refresh_gui_buddylist(); /* update the UI */

  return TRUE;
}


/*
  updates given buddy if found and returns TRUE,
  or returns FALSE on non-existent buddy
*/

gboolean ui_buddy_update_by_fp(gchar *str_fingerprint) {

  debug("updating a buddy by fp '%s'...", str_fingerprint);
  SilkyBuddy *b = buddy_find_by_fp(str_fingerprint);

  if (b) {
    ui_buddy_update(b);
    return TRUE;
  } else {
    return FALSE;
  }

}




gboolean ui_buddy_update_foreachfunc (GtkTreeModel *model,
					 GtkTreePath *path,
					 GtkTreeIter *childiter,
					 gpointer bx) {
  SilkyBuddy *b;
  SilkyBuddy *lb = bx;
  GtkTreeStore *treestore;

  debug("foreachfunc...");

  gtk_tree_model_get (model, childiter, BUDDYLIST_COL_BUDDYPTR, &b, -1);

  if (b != lb) {
    return FALSE;
  }

  debug("New nickname: '%s'", b->nickname);

  treestore = GTK_TREE_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(glade_xml_get_widget(xmlmain, "buddylist_treeview"))));

   /* ICON column */
   if (!CONNECTED) {
     debug("set unknown icon as we are not connected");
     gtk_tree_store_set(treestore, childiter, BUDDYLIST_COL_ICON, GTK_STOCK_DIALOG_QUESTION, -1); /* column 0, status image */
   } else {
     switch ( b->mode) {

     case SILC_UMODE_DETACHED:
       gtk_tree_store_set(treestore, childiter, BUDDYLIST_COL_ICON, GTK_STOCK_DIALOG_WARNING, -1); /* column 0, status image */
       break;

     case SILC_UMODE_GONE:
       gtk_tree_store_set(treestore, childiter, BUDDYLIST_COL_ICON, GTK_STOCK_DIALOG_INFO, -1); /* column 0, status image */
       break;

     default:
       /* all other modes use the network icon */
       gtk_tree_store_set(treestore, childiter, BUDDYLIST_COL_ICON, GTK_STOCK_JUMP_TO, -1); /* column 0, status image */
       break;
     }
   }

   /* store pointer to the buddy for later use */
   gtk_tree_store_set(treestore, childiter, BUDDYLIST_COL_BUDDYPTR, b, -1); /* SilkyBuddy */

   /* nickname */
   gtk_tree_store_set(treestore, childiter, BUDDYLIST_COL_NAME, b->nickname, -1);

   return TRUE;
}


gboolean ui_buddy_update (SilkyBuddy *b) {
  GtkTreeStore *treestore;


  debug("updating a buddy...");

  treestore = GTK_TREE_STORE(gtk_tree_view_get_model(GTK_TREE_VIEW(glade_xml_get_widget(xmlmain, "buddylist_treeview"))));

  g_assert(treestore);

  gtk_tree_model_foreach(GTK_TREE_MODEL(treestore), *ui_buddy_update_foreachfunc, (gpointer)b);

  return TRUE;
}




/*
  Called from GTK when user selects an user from the buddylist.
  Shows a popup list if right-mouse-button was pressed
*/
gboolean buddy_selected (GtkTreeView *treeview, GdkEventButton *event) {
  GtkTreeSelection *selection;
  GtkTreeIter iter;
  GtkTreeModel *model;
  GtkTreePath *path;

  SilkyBuddy *b;
  GtkMenu *menu;
  GtkWidget *menuitem;
  const gchar *pubkeyfile;

  /* right click */
  if (event && event->button && event->button == 3) {
    if( gtk_tree_view_get_path_at_pos(treeview, (gint)event->x, (gint)event->y, &path, NULL, NULL, NULL) ) {

      selection = gtk_tree_view_get_selection(treeview);

      gtk_tree_selection_unselect_all(selection);
      gtk_tree_selection_select_path(selection, path);

      /* popup the menu only if something was selected */
      if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
	gtk_tree_model_get (model, &iter, BUDDYLIST_COL_BUDDYPTR, &b, -1);
	if (!b) {
	  debug("not a buddy");
	  return FALSE;
	}
	debug("selected nickname '%s', fingerprint '%s'", b->nickname, b->fingerprint);

	/* create the menu */
	menu = GTK_MENU(gtk_menu_new());

	if (b->client_id) {
	  debug("client_id found");
	} else {
	  debug("client_id not yet cached");
	}


	/* ADD MENU items and signal handlers*/

	/* WHOIS */

	menuitem = gtk_menu_item_new_with_label(_("Who is"));

	if (silky->conn) {
	  if( b->client_id ) {
	    g_signal_connect(menuitem, "activate", G_CALLBACK(on_command_whois), b->client_id);
	  } else if( b->fingerprint ) {
	    pubkeyfile = gen_keyfile_path(b->fingerprint, "client");
	    g_signal_connect(menuitem, "activate", G_CALLBACK(on_command_whois_pubkey), (gpointer *)pubkeyfile);
	  }
	  gtk_widget_set_sensitive(menuitem, TRUE);
	} else {
	  gtk_widget_set_sensitive(menuitem, FALSE);
	}


	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	if (!silky->conn) { /* disable item if we are not connected */
	  gtk_widget_set_sensitive(menuitem, FALSE);
	}
	gtk_widget_show (menuitem);


	/* MSG */
	menuitem = gtk_menu_item_new_with_label(_("Start conversation"));

	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	if (!silky->conn || !b->client_id) {
	  gtk_widget_set_sensitive(menuitem, FALSE);
	} else {
	  gtk_widget_set_sensitive(menuitem, TRUE);
	  g_signal_connect(menuitem, "activate", G_CALLBACK(on_command_query), b->client_id);
	}
	gtk_widget_show (menuitem);


	/* a separator here */
	menuitem = gtk_separator_menu_item_new();
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show(menuitem);

	/* remove from buddylist */
	menuitem = gtk_menu_item_new_with_label(_("Remove Buddy"));
	g_signal_connect(menuitem, "activate", G_CALLBACK(ui_buddy_remove), b->fingerprint);
	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menuitem);
	gtk_widget_show (menuitem);

	/* show the popup menu */
	gtk_menu_popup (menu, NULL, NULL, NULL, NULL,
			event->button, event->time);
      }
    }
  }

  return FALSE; /* tell GTK to use selector */


}

gint buddylist_iter_get_type(GtkTreeModel *model, GtkTreeIter iter) {
	gint type;

	gtk_tree_model_get(model, &iter, BUDDYLIST_COL_TYPE, &type, -1);
	return type;
}

gboolean on_buddylist_treeview_row_activated(GtkTreeView *treeview,
		GtkTreePath *tp, GtkTreeViewColumn *col, gpointer user_data) {
	GtkTreeIter act;
	GtkTreePath *path;
	GtkTreeModel *model;

	model = gtk_tree_view_get_model(treeview);
	gtk_tree_model_get_iter(model, &act, tp);
	path = gtk_tree_model_get_path(model, &act);
	switch( buddylist_iter_get_type(model, act) ) {
		case BUDDYLIST_TYPE_GROUP:
			debug("Buddylist item type is GROUP");
			if( gtk_tree_view_row_expanded(treeview, path) )
				gtk_tree_view_collapse_row(treeview, path);
			else gtk_tree_view_expand_row(treeview, path, FALSE);
			break;
		case BUDDYLIST_TYPE_BUDDY:
			debug("Buddylist item type is BUDDY");
			debug("FIXME: should open query window if online");
			break;
		default:
			debug("WARNING: Unhandled buddylist item type!");
			break;
	}

	return FALSE;
}
