/*
    Silky - A GTK+ client for SILC.
    Copyright (C) 2003, 2004, 2005 Toni Willberg

    - All log and debug -related stuff lives here.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    http://silky.sourceforge.net/

*/

#include <glib.h>

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "log.h"

extern gint debug_on;


/* the client library might call this on unhandled(?) situations */
bool silky_silc_log_callback(SilcLogType type, gchar *message, void *context) {
  debug_message("SILCLOG: %s", message);
  return TRUE;
}

/**
 * debug_real:
 * @file:
 * @line:
 * @function:
 * @msg: message string
 * @Varargs: message continues
 *
 * Called by debug()
 *
 * @Returns: void
 **/


/*
  the actual debug function
  _always_ prints LOG_ERROR level messages, others only on demand
*/
void debug_real (const SilkyLogLevel type, const char *file, const int line, const char *function, const char *msg, ...) {
  va_list va;
    
  /* return if this debug message is too high for given debug_level */
  if ( type > debug_level && type != LOG_ERROR )
    return;

  if ( debug_on != 1 && type != LOG_ERROR )
    return;

  //  char str[4096];
  va_start(va, msg);
  //  g_vsnprintf(str, sizeof(str)-1, msg, va);
  //  fprintf(stderr, "%s:%d, %s(): %s\n", file, line, function, str)
  fprintf(stderr, "%s:%d:%s()\t\t", file, line, function);
  vfprintf(stderr, msg, va);
  fprintf(stderr, "\n");
  va_end(va);
  fflush(stderr);			/* Make sure the message prints. */

  return;

} /* End of debug_real() */


/**
 * silky_print_stderr:
 * @log_domain: A string, fe. "libglade"
 * @log_level:
 * @message:
 * @user_data:
 *
 * This is called whenever g* functions want to print something to STDERR.
 *
 * @Returns: void
 *
 **/

void silky_print_stderr (const gchar *log_domain,
			 GLogLevelFlags log_level,
			 const gchar *message,
			 gpointer user_data) {

  debug("%s: %s", log_domain, message);

}
