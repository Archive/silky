
/**
 * debug:
 * @str: A message
 * @ARGS...: Arguments
 *
 * Used to print debug to STDOUT
 *
 * @Returns:
 **/

#ifndef LOG_H__
#define LOG_H__

#include <silcincludes.h>

typedef enum {
  LOG_ERROR = -1,
  LOG_MESSAGE = 0,
  LOG_INFO = 1,
  LOG_DEBUG = 2
} SilkyLogLevel;

GLogLevelFlags debug_level;

#define debug_message(str, ARGS...)	debug_real(LOG_MESSAGE, __FILE__, __LINE__, __FUNCTION__, str, ##ARGS)
#define debug_info(str, ARGS...)	debug_real(LOG_INFO, __FILE__, __LINE__, __FUNCTION__, str, ##ARGS)
#define debug_error(str, ARGS...)	debug_real(LOG_ERROR, __FILE__, __LINE__, __FUNCTION__, str, ##ARGS)
#define debug(str, ARGS...)	debug_real(LOG_DEBUG, __FILE__, __LINE__, __FUNCTION__, str, ##ARGS)


bool silky_silc_log_callback(SilcLogType type, gchar *message, void *context);


void debug_real (const SilkyLogLevel, const char *, const int, const char *, const char *, ...);
void silky_print_stderr (const gchar *log_domain, GLogLevelFlags log_level, const gchar *message, gpointer user_data);


#endif
