#ifdef ENABLE_NLS
#include <libintl.h>
#define _(str)  gettext(str)
#define N_(str)  str
#else
#define _(str)  str
#define N_(str)  str
#define ngettext(str1, str2, arg) str2
#endif
