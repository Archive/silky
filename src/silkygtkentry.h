/*
    Silky - A GTK+ client for SILC.
    Copyright (C) 2003, 2004, 2005 Toni Willberg
    
    - GtkEntry with added history

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    http://silky.sourceforge.net/

*/

#ifndef __SILKYGTKENTRY_H__
#define __SILKYGTKENTRY_H__

#include <glib.h>
#include <gtk/gtk.h>

#include <stdio.h>

#include "log.h"

G_BEGIN_DECLS

#define TYPE_SILKY_GTK_ENTRY		(silky_gtk_entry_get_type())
#define SILKY_GTK_ENTRY(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), TYPE_SILKY_GTK_ENTRY, SilkyGtkEntry))
#define SILKY_GTK_ENTRY_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), TYPE_SILKY_GTK_ENTRY, SilkyGtkEntryClass))
#define IS_SILKY_GTK_ENTRY(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), TYPE_SILKY_GTK_ENTRY))
#define IS_SILKY_GTK_ENTRY_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), TYPE_SILKY_GTK_ENTRY))
#define SILKY_GTK_ENTRY_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), TYPE_SILKY_GTK_ENTRY, SilkyGtkEntryClass))

typedef struct _SilkyGtkEntry		SilkyGtkEntry;
typedef struct _SilkyGtkEntryClass	SilkyGtkEntryClass;

struct _SilkyGtkEntry
{
  GtkEntry		entry;		/* parent widget */

  gint			history_size;

  GList			*cmd_history;
  GList			*current;
};

struct _SilkyGtkEntryClass
{
  GtkEntryClass		parent_class;
};

GType 		silky_gtk_entry_get_type		(void) G_GNUC_CONST;
GtkWidget 	*silky_gtk_entry_new			(gint history_size);
void		silky_gtk_entry_set_history_size	(SilkyGtkEntry *silky_entry, gint size);
gint		silky_gtk_entry_get_history_size	(SilkyGtkEntry *silky_entry);
gchar 		*silky_gtk_entry_next_history_item	(SilkyGtkEntry *silky_entry);
gchar 		*silky_gtk_entry_prev_history_item	(SilkyGtkEntry *silky_entry);

void		silky_gtk_entry_history_push		(SilkyGtkEntry *silky_entry, gchar *text);

void		silky_gtk_entry_reset_browsing		(SilkyGtkEntry *silky_entry);

G_END_DECLS

#endif
