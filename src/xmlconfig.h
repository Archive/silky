#ifndef XMLCONFIG_H__
#define XMLCONFIG_H__

#include <libxml/parser.h>
#include <libxml/xpath.h>

xmlNodePtr xml_create_node(gint config_type, gint node_cn);
xmlNodePtr xml_get_node(gint config_type, xmlDocPtr doc, gint node_cn);

xmlDocPtr xml_read_config(gint config_type, const char *filename);
gint xml_save_config(gint config_type, const char *filename);
xmlDocPtr xml_create_config(gint config_type);
void xml_cleanup_config(gint config_type);

gchar *xml_get_option(gint config_type, gint node_cn);
gint xml_set_option(gint config_type, gint node_cn, xmlChar *value);
gint xml_find_option_num_by_name(gint config_type, xmlChar *name);

gint xml_write_config(gint config_type, const char *filename);

xmlDocPtr xml_doc(gint type);

#endif
