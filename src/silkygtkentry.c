/*
    Silky - A GTK+ client for SILC.
    Copyright (C) 2003, 2004, 2005 Toni Willberg
    
    - GtkEntry widget with added history

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    http://silky.sourceforge.net/

*/

#include "silkygtkentry.h"

#define DEFAULT_HISTORY_SIZE	20
#define MAX_HISTORY_SIZE	200

enum {
  PROP_0,
  PROP_HISTORY_SIZE
};

static void silky_gtk_entry_class_init (SilkyGtkEntryClass *klass);
static void silky_gtk_entry_init (SilkyGtkEntry *entry);

static void silky_gtk_entry_set_property (GObject 	*object,
					guint		prop_id,
					const GValue	*value,
					GParamSpec	*pspec);
static void silky_gtk_entry_get_property (GObject 	*object,
					guint		prop_id,
					GValue	*value,
					GParamSpec	*pspec);
static void silky_gtk_entry_finalize (GObject *object);

static GObjectClass *parent_class = NULL;

GType silky_gtk_entry_get_type (void)
{
  static GType silky_gtk_entry_type = 0;

  if( !silky_gtk_entry_type ) {
    static const GTypeInfo silky_gtk_entry_info = {
      sizeof (SilkyGtkEntryClass),
      NULL,
      NULL,
      (GClassInitFunc) silky_gtk_entry_class_init,
      NULL,
      NULL,
      sizeof (SilkyGtkEntry),
      0,
      (GInstanceInitFunc) silky_gtk_entry_init,
    };

    silky_gtk_entry_type = g_type_register_static(GTK_TYPE_ENTRY,
						    "SilkyGtkEntry",
						    &silky_gtk_entry_info,
						    0);
  }

  return silky_gtk_entry_type;
}

static void
silky_gtk_entry_class_init (SilkyGtkEntryClass *klass)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS(klass);

  parent_class = g_type_class_peek_parent (klass);

  gobject_class->set_property = silky_gtk_entry_set_property;
  gobject_class->get_property = silky_gtk_entry_get_property;
  gobject_class->finalize = silky_gtk_entry_finalize;

  g_object_class_install_property (gobject_class,
				    PROP_HISTORY_SIZE,
				    g_param_spec_int ("history_size",
							 "History Size",
							 "Number of history items for this entry",
							 0,
							 MAX_HISTORY_SIZE,
							 DEFAULT_HISTORY_SIZE,
							 G_PARAM_READWRITE));
}

static void
silky_gtk_entry_init (SilkyGtkEntry *entry)
{
  entry->history_size = DEFAULT_HISTORY_SIZE;
  entry->cmd_history = NULL;
  entry->current = NULL;
}

static void
silky_gtk_entry_set_property (	GObject		*object,
				guint		prop_id,
				const GValue	*value,
				GParamSpec	*pspec)
{
  SilkyGtkEntry *entry;

  entry = SILKY_GTK_ENTRY (object);

  switch (prop_id) {
    case PROP_HISTORY_SIZE:
      silky_gtk_entry_set_history_size (entry, g_value_get_int (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
silky_gtk_entry_get_property (	GObject		*object,
				guint		prop_id,
				GValue		*value,
				GParamSpec	*pspec)
{
  SilkyGtkEntry *entry = SILKY_GTK_ENTRY (object);

  switch (prop_id) {
    case PROP_HISTORY_SIZE:
      g_value_set_int (value, entry->history_size);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
silky_gtk_entry_finalize (GObject *object)
{
  SilkyGtkEntry *entry = SILKY_GTK_ENTRY(object);

  /* free all history items */
  g_list_free(entry->cmd_history);

  entry->cmd_history = NULL;
  entry->current = NULL;
}

GtkWidget *
silky_gtk_entry_new (gint history_size)
{
  return g_object_new (TYPE_SILKY_GTK_ENTRY, "history_size", history_size, NULL);
}

void
silky_gtk_entry_set_history_size (SilkyGtkEntry	*entry,
				  gint		size)
{
  g_return_if_fail (IS_SILKY_GTK_ENTRY(entry));

  size = CLAMP (size, 0, MAX_HISTORY_SIZE);

  /* FIXME: chop off surplus history items if necessary */
  while( g_list_length(entry->cmd_history) > size ) g_list_delete_link(entry->cmd_history, g_list_last(entry->cmd_history));

  entry->history_size = size;
}

gint
silky_gtk_entry_get_history_size (SilkyGtkEntry *entry)
{
  g_return_val_if_fail (GTK_IS_ENTRY (entry), 0);

  return entry->history_size;
}

gchar *
silky_gtk_entry_next_history_item (SilkyGtkEntry *entry)
{
  g_return_val_if_fail (IS_SILKY_GTK_ENTRY(entry), "");

  if( !entry->cmd_history ) return "";
  if( !entry->current ) return "";

  entry->current = entry->current->prev;

  if( entry->current && entry->current->data && g_utf8_strlen(entry->current->data, -1) )
    gtk_entry_set_text(GTK_ENTRY(entry), entry->current->data);
  else
    gtk_entry_set_text(GTK_ENTRY(entry), "");

  gtk_editable_set_position(GTK_EDITABLE(entry), -1);
  gtk_editable_select_region(GTK_EDITABLE(entry), 0,0);

  if( entry->current && entry->current->data )
    return (gchar *)entry->current->data;
  else
    return "";
}

gchar *
silky_gtk_entry_prev_history_item (SilkyGtkEntry *entry)
{
  g_return_val_if_fail (IS_SILKY_GTK_ENTRY(entry), "");

  if( !entry->cmd_history ) return "";

  if( !entry->current ) /* we were not browsing history until now */
    entry->current = entry->cmd_history;
  else	/* we were browsing history, just move the pointer */
    if( entry->current->next ) entry->current = entry->current->next;
      else return (gchar *)entry->current->data;

  if( entry->current && entry->current->data && g_utf8_strlen(entry->current->data, -1) )
    gtk_entry_set_text(GTK_ENTRY(entry), entry->current->data);
  else
    gtk_entry_set_text(GTK_ENTRY(entry), "");

  gtk_editable_set_position(GTK_EDITABLE(entry), -1);
  gtk_editable_select_region(GTK_EDITABLE(entry), 0,0);

  if( entry->current && entry->current->data )
    return (gchar *)entry->current->data;
  else
    return "";
}

void
silky_gtk_entry_history_push (SilkyGtkEntry *entry,
				gchar *text)
{
  g_return_if_fail (IS_SILKY_GTK_ENTRY(entry));

  if( !entry->cmd_history ) debug("HISTORY NULL!");

  debug("adding '%s'", text);

  if( !g_utf8_validate(text, g_utf8_strlen(text, -1), NULL) ) {
    debug("non-utf8 string passed as argument");
    return;
  }

/* first, let's test if entered command is the same as last entry in history.
   no point in having it added twice */
  if( entry->cmd_history && !strcmp(text, (gchar *)g_list_first(entry->cmd_history)->data) )
    return;

/* now let's add newest entry to the beginning, so cmd_history points to it */
  entry->cmd_history = g_list_prepend(entry->cmd_history, text);

/* if addition was succesful, let's expire oldest entry */
  while( g_list_length(entry->cmd_history) > entry->history_size )
    g_list_delete_link(entry->cmd_history, g_list_last(entry->cmd_history));
}

void
silky_gtk_entry_reset_browsing (SilkyGtkEntry *entry) {
  entry->current = NULL;
}
