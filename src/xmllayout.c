/*
    Silky - A GTK+ client for SILC.
    Copyright (C) 2003, 2004, 2005 Toni Willberg

    - Arrays defining layout of xml config files

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    http://silky.sourceforge.net/

*/

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif
#include <string.h>


#include "silcincludes.h"
#include "silcclient.h"

/* some of our support functions */
#include "support.h"

/* config files */
#include "xmlconfig.h"
#include "xmllayout.h"
#include "common.h"


/* arrays for xml elements and attributes definitions follow... */

/* attribute number,	name used in xml,	default value */
SilkyXMLAttribute xml_attributes[] = {
 { ATTR_VERSION,	"version",		SILKY_VERSION },
 { ATTR_CONFIG_VERSION, "config-version",	SILKY_CONFIG_VERSION },
};

gint xml_attr_bits[] = {
  ATTR_VERSION,
  ATTR_CONFIG_VERSION,
};


/* { name,		type,		parent,		additional attributes,
									default value

Keep this in same order as enums

 */

/* using number of main config elements here, because it's largest */
SilkyXMLElement xml_elements[NUM_CONFIG][NUM_CM] = {

/**************** MAIN */
{
 { NULL,		0,			0,	NULL },
 { (xmlChar *)"silky",		0,			ATTR_VERSION | ATTR_CONFIG_VERSION,	NULL },

 { (xmlChar *)"userinfo",		CM_ROOT,		0, NULL },
 { (xmlChar *)"nickname",  	CM_USERINFO,		0, "" },	/* empty, system should provide this */
 { (xmlChar *)"realname",  	CM_USERINFO,		0, "Silky User" },
 { (xmlChar *)"quitmessage",	CM_USERINFO,		0, N_("Another happy Silky user") },

 { (xmlChar *)"gui",		CM_ROOT,		0, NULL },
 { (xmlChar *)"colors",		CM_GUI,			0, NULL },
 { (xmlChar *)"mynickname",	CM_GUI_COLORS,		0, "default" },
 { (xmlChar *)"mytext",		CM_GUI_COLORS,		0, "default" },
 { (xmlChar *)"myaction",		CM_GUI_COLORS,		0, "default" },
 { (xmlChar *)"nicknames",		CM_GUI_COLORS,		0, "default" },
 { (xmlChar *)"text",		CM_GUI_COLORS,		0, "default" },
 { (xmlChar *)"actions",		CM_GUI_COLORS,		0, "default" },
 { (xmlChar *)"timestamp",		CM_GUI_COLORS,		0, "default" },
 { (xmlChar *)"servermessages",	CM_GUI_COLORS,		0, "default" },
 { (xmlChar *)"inputbox",		CM_GUI_COLORS,		0, "default" },
 { (xmlChar *)"background",	CM_GUI_COLORS,		0, "default" },
 { (xmlChar *)"highlight-words",	CM_GUI_COLORS,		0, "default" },
 { (xmlChar *)"highlight-mynick",	CM_GUI_COLORS,		0, "default" },

 { (xmlChar *)"highlights",	CM_GUI,			0, NULL },
 { (xmlChar *)"own-nick",		CM_GUI_HIGHLIGHTS,	0, "ON" },
 { (xmlChar *)"words",		CM_GUI_HIGHLIGHTS,	0, "" },

 { (xmlChar *)"font",		CM_GUI,			0, "" },
 { (xmlChar *)"debug-usec",	CM_GUI,			0, "OFF" },
 { (xmlChar *)"cmdhistory-depth",	CM_GUI,			0, "50" },
 { (xmlChar *)"tips",		CM_GUI,			0, "ON" },

 { (xmlChar *)"security",		CM_ROOT,		0, NULL },
 { (xmlChar *)"message-signing",	CM_SECURITY,		0, NULL },
 { (xmlChar *)"channel-messages",	CM_SECURITY_MESSAGE_SIGNING,	0,	"ON" },
 { (xmlChar *)"private-messages",	CM_SECURITY_MESSAGE_SIGNING,	0,	"ON" },
 { (xmlChar *)"channel-actions",	CM_SECURITY_MESSAGE_SIGNING,	0,	"ON" },
 { (xmlChar *)"private-actions",	CM_SECURITY_MESSAGE_SIGNING,	0,	"ON" },
},

/****** SERVERS */
{
 { NULL,		0,			0, NULL },
 { (xmlChar *)"silky",		0,			ATTR_VERSION | ATTR_CONFIG_VERSION, NULL },

 { (xmlChar *)"server",		CS_ROOT,		0, NULL },
 { (xmlChar *)"hostname",		CS_SERVER,		0, "" },
 { (xmlChar *)"port",		CS_SERVER,		0, "706" },
 { (xmlChar *)"autoconnect",	CS_SERVER,		0, "OFF" },
 { (xmlChar *)"passphrase",	CS_SERVER,		0, "" },
},
/****** BUDDIES */
{
 { NULL,		0,			0, NULL },
 { (xmlChar *)"silky",		0,			ATTR_VERSION | ATTR_CONFIG_VERSION, NULL },

 { (xmlChar *)"buddy",		CB_ROOT,		0, NULL },
 { (xmlChar *)"nickname",		CB_BUDDY,		0, "" },
 { (xmlChar *)"realname",		CB_BUDDY,		0, "" },
 { (xmlChar *)"fingerprint",	CB_BUDDY,		0, "" },
 { (xmlChar *)"keyverified",	CB_BUDDY,		0, "" },
 { (xmlChar *)"notes",		CB_BUDDY,		0, "" },

 { (xmlChar *)"attributes",	CB_BUDDY,		0, NULL },
 { (xmlChar *)"languages",		CB_BUDDY_ATTR,		0, "" },
 { (xmlChar *)"preferred-contact", CB_BUDDY_ATTR,		0, "" },
 { (xmlChar *)"timezone",		CB_BUDDY_ATTR,		0, "" },
 { (xmlChar *)"geolocation",	CB_BUDDY_ATTR,		0, "" }

}
};
