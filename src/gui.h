#ifndef GUI_H__
#define GUI_H__

#include "channel.h"
#include "support.h"


void silky_gui_show_main();
void silky_gui_check_channel_cmodes();

void silky_print(gint type, void *target, gchar *text, ...);
void silky_print_channel(gint type, SilkyChannel *channel, gchar *string);
void silky_print_buffer(GtkTextView *textview, gchar *text);

gint gui_tab_type(gint pagenr);
void gui_renumber_tabs();

void set_textview_colors(GtkTextView *widget_textview);

void close_all_tabs( void );
int printconsole (const char *msg);

void silky_show_user_info(SilcClientEntry user);
gboolean silky_update_user_info(GtkWidget *dialog);

void on_ui_send_check(GtkWidget *widget, gpointer input);

GtkWidget *silky_get_active_inputbox(void);
void ui_show_console();

typedef enum {
  MSG_PUBLIC	= 1<<0,
  MSG_PRIVATE	= 1<<1,

  MSG_CONSOLE	= 1<<2,
  MSG_ALL	= 1<<3,

  MSG_MESSAGE	= 1<<4,
  MSG_ACTION	= 1<<5,
  MSG_MOTION	= 1<<6,

  MSG_SERVER	= 1<<7,
  MSG_INFO	= 1<<8,

  MSG_SIGNED	= 1<<9
} SilkyGUIMessageFlags;

#define template_signed		"[%s]%s"
#define template_motion		"*** %s"
#define template_timestamp	"%s | %s"

enum {
  TAB_TYPE_CONSOLE,
  TAB_TYPE_CHANNEL,
  TAB_TYPE_QUERY,
  TAB_TYPE_UNKNOWN,
  NUM_TAB_TYPES
};

#define USERINFO_UPDATE_PERIOD	2000

gboolean scheduled_window_hiding (GtkWindow *);

void _errordialog(const char *, GtkWindow *);
void dialog_cannot_connect_to_server (GtkWindow *window);
void dialog_not_connect_to_server ();
void dialog_nothing_matched_search_found (GtkWindow *window);
void dialog_passphrase_mismatch(GtkWindow*);
void dialog_passphrase_incorrect (GtkWindow*);

void disconnect_from_server();
void control_c(int s);

void update_presence_button();
void presence_menu_set_sensitivity(GtkWidget *widget, gpointer state);

#define SILKY_CLIENT_OFFLINE		N_("Offline")
#define SILKY_CLIENT_ONLINE		N_("Online")
#define SILKY_CLIENT_CONNECTING		N_("Connecting, please wait...")
#define SILKY_CLIENT_DISCONNECTING	N_("Disconnecting...")
#define SILKY_CLIENT_GONE		N_("Gone")
#define SILKY_CLIENT_INDISPOSED		N_("Indisposed")
#define SILKY_CLIENT_BUSY		N_("Busy")
#define SILKY_CLIENT_PAGE		N_("Page me")
#define SILKY_CLIENT_HYPER		N_("Hyperactive!")

#endif
