#ifndef COMMANDS_H__
#define COMMANDS_H__

typedef int (*cmd_callback) (char *tbuf, char *word[], char *word_eol[]);

struct commands
{
	char *name;
	cmd_callback callback;
	char needserver;
	char needchannel;
	char *help;
};

extern const struct commands xc_cmds[];

void command_help(char *tbuf, char *helpcmd, int quiet);
gboolean handle_command_callback (GtkWidget *emitter, gchar *cmd);
int handle_command(char *cmd);

void process_data_init (char *buf, char *cmd, char *word[], char *word_eol[], int handle_quotes);
void notj_msg (void);
void notc_msg (void);
#define PDIWORDS		32

void silky_change_umode(SilcUInt32 umode);

#endif
