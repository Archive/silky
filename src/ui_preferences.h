#ifndef UI_PREFERENCES_H__
#define UI_PREFERENCES_H__


/*
  column identifiers for server list in preferences
*/
enum { /* 0..n */
  srv_col_hostname = 1,
  srv_col_port = 2,
  srv_col_autoconnect = 0
};

void prefs_clear_passphrase_dialog();
void cb_prefs_change_passphrase(GtkWidget*, gpointer);
void populate_prefs_serverlist();
void gui_prefs_set_up_server_treeview();
void on_settings_server_add(GtkWidget *widget, gpointer user_data);
void on_settings_server_remove(GtkWidget *widget, gpointer user_data);
gboolean silky_tips_toggle (GtkWidget *widget, gpointer ptr);

#endif
