
void silc_say(SilcClient client, SilcClientConnection conn,
                     SilcClientMessageType type, char *msg, ...);

void
silc_channel_message(SilcClient client, SilcClientConnection conn,
                     SilcClientEntry sender, SilcChannelEntry channel,
                     SilcMessagePayload payload,
		     SilcChannelPrivateKey key,
                     SilcMessageFlags flags, const unsigned char *message,
                     SilcUInt32 message_len);

void
silc_private_message(SilcClient client, SilcClientConnection conn,
                     SilcClientEntry sender, SilcMessagePayload payload,
                     SilcMessageFlags flags,
                     const unsigned char *message,
                     SilcUInt32 message_len);

void silc_command(SilcClient client, SilcClientConnection conn,
                         SilcClientCommandContext cmd_context, bool success,
                         SilcCommand command, SilcStatus status);


void silc_connected(SilcClient client, SilcClientConnection conn,
                           SilcClientConnectionStatus status);

void silc_disconnected(SilcClient client, SilcClientConnection conn,
                              SilcStatus status, const char *msg);

void silc_get_auth_method(SilcClient client, SilcClientConnection conn,
                                 char *hostname, SilcUInt16 port,
                                 SilcGetAuthMeth completion, void *context);


void silc_verify_public_key(SilcClient client, SilcClientConnection conn,
                                   SilcSocketType conn_type, unsigned char *pk,
                                   SilcUInt32 pk_len, SilcSKEPKType pk_type,
                                   SilcVerifyPublicKey completion, void *ctx);

void silc_ask_passphrase(SilcClient client, SilcClientConnection conn,
                                SilcAskPassphrase completion, void *context);

void silc_failure(SilcClient client, SilcClientConnection conn,
                         SilcProtocol proto, void *failure);

bool silc_key_agreement(SilcClient client, SilcClientConnection conn,
                               SilcClientEntry entry, const char *hostname,
                               SilcUInt16 port,
                               SilcKeyAgreementCallback *completion,
                               void **context);

void silc_ftp(SilcClient client, SilcClientConnection conn,
                     SilcClientEntry entry, SilcUInt32 session_id,
                     const char *hostname, SilcUInt16 port);

void silc_detach(SilcClient client, SilcClientConnection conn,
                        const unsigned char *detach_data,
                        SilcUInt32 data_len);

SilcClientOperations ops;


/* ### */
