/*
    Silky - A GTK+ client for SILC.
    Copyright (C) 2003,2004 Toni Willberg

    - Query list management functions

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    http://silky.sourceforge.net/

*/

#include <gtk/gtk.h>
#include <silcincludes.h>
#include <silcclient.h>

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "query.h"
#include "log.h"

extern GladeXML *xmlmain;
SilkyQuery *queries = NULL, *query_active = NULL;

SilkyQuery *query_find_by_cliententry( SilcClientEntry cliententry ) {
  SilkyQuery *q;

  if( !cliententry ) {
    return NULL;
  }

  for( q = queries; q; q = q->next ) {
    if( (cliententry == q->client_entry) ) {
      debug("Found query.");
      return q;
    }
  }

  debug("Query not found, returning NULL.");
  return NULL;
}

SilkyQuery *query_find_by_pagenr( guint pagenr ) {
  SilkyQuery *q;

  for( q = queries; q; q = q->next ) {
    if( q->pagenr == pagenr ) {
      debug("Found query, page %d", q->pagenr);
      return q;
    }
  }
  debug("Query not found on channel %d.", pagenr);
  return NULL;
}

SilkyQuery *query_add( SilcClientEntry cliententry ) {
  SilkyQuery *q, *newq;

  if( query_find_by_cliententry(cliententry) ) {
    debug("Query already exists for '%s', returning", cliententry->nickname);
    return NULL;
  }

  debug("Creating SilkyQuery struct for '%s'", cliententry->nickname);
  newq = malloc( sizeof( SilkyQuery ) );
  newq->next = NULL;
  newq->client_entry = cliententry;
  newq->pagenr = -1;	/* will be set later */
  newq->label_text = NULL;
  newq->label_image = NULL;

  if( !queries ) {
    debug("First query, creating linked list");
    queries = newq;
  } else {
    debug("There are already some queries, appending to linked list");
    for( q = queries; q->next; q = q->next ) {}
    q->next = newq;
  }

  return newq;
}

void query_pagenr_set_by_cliententry( SilcClientEntry cliententry, int pagenr ) {
  SilkyQuery *q = query_find_by_cliententry(cliententry);

  if(q) {
    debug("setting pagenr for a query tab");
    q->pagenr = pagenr;
  }
}

gboolean query_remove( SilkyQuery *q ) {
  SilkyQuery *prevq, *x;
  guint found = 0;

  if( !queries || !q ) {
    debug("No queries, returning");
    return FALSE;
  }

  for( x = queries, prevq = queries; q; x = x->next ) {
    if( (x == q) ) {
      debug("Found query");
      found++;
      break;
    }
    prevq = x;
  }

  if( (queries == x) ) {
    debug("It is first query");
    queries = queries->next;
  } else {
    debug("Not first query");
    prevq->next = x->next;
  }

  debug("Removing query tab from GUI");
  gtk_notebook_remove_page(GTK_NOTEBOOK(glade_xml_get_widget(xmlmain, "tabs")), q->pagenr);

  debug("freeing query struct");
  free(q);
  return TRUE;
}

gboolean query_remove_by_cliententry( SilcClientEntry cliententry ) {
  SilkyQuery *q, *prevq;
  guint found = 0;

  if( !queries ) {
    debug("No queries, returning");
    return FALSE;
  }

  for( q = queries, prevq = queries; q; q = q->next ) {
    if( (cliententry == q->client_entry) ) {
      debug("Found query");
      found++;
      break;
    }
    prevq = q;
  }

  if( !found ) {
    debug("Could not find query, returning");
    return FALSE;
  }

  if( (queries == q) ) {
    debug("It is first query");
    queries = queries->next;
  } else {
    debug("Not first query");
    prevq->next = q->next;
  }

  debug("freeing query struct");
  free(q);
  return TRUE;
}

gboolean query_remove_by_pagenr( guint pagenr ) {
  SilkyQuery *q, *prevq;
  guint found = 0;

  if( !queries ) {
    debug("No queries, returning");
    return FALSE;
  }

  for( q = queries, prevq = queries; q; q = q->next ) {
    if( (pagenr == q->pagenr) ) {
      debug("Found query");
      found++;
      break;
    }
    prevq = q;
  }

  if( !found ) {
    debug("Could not find query, returning");
    return FALSE;
  }

  if( (queries == q) ) {
    debug("It is first query");
    queries = queries->next;
  } else {
    debug("Not first query");
    prevq->next = q->next;
  }

  debug("freeing query struct");
  free(q);
  return TRUE;
}

void query_free_all() {
  SilkyQuery *q, *prevq = NULL;

  if( !queries ) {
    debug("no queries");
    return;
  }

  for( q = queries; q; q = q->next ) {
    if(prevq) free(prevq);
    prevq = q;
    if( q && !q->next ) {
      free(q);
      break;
    }
  }

  queries = NULL;
}
