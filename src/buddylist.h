#ifndef BUDDYLIST_H__
#define BUDDYLIST_H__

#include <silcincludes.h>


struct silkybuddy_t {
  struct silkybuddy_t	*next;
  gchar			*nickname;
  gchar			*realname;
  gchar			*fingerprint;
  gint			keyverified;

  gchar			*languages;
  gchar			*preferred_contact;
  gchar			*timezone;
  gchar			*geolocation;

  gchar			*notes;

  SilcClientID		*client_id;
  SilcUInt32 mode;
};

typedef struct silkybuddy_t SilkyBuddy;

gboolean init_buddies(gchar *filename);
gboolean parse_buddies();
gboolean save_buddies();

SilkyBuddy *buddy_find_by_nick(gchar *nick);
SilkyBuddy *buddy_find_by_fp(gchar *fingerprint);

gboolean buddy_add(gchar *, gchar *fingerprint);
gboolean watch_add_fingerprint(gchar *);
void watch_add(gchar *);
void watch_del(gchar *);
void buddy_init_watchlist();

gboolean buddy_set_mode_by_fp(gchar *fingerprint, SilcUInt32);
gboolean buddy_set_nick_by_fp(gchar *fingerprint, gchar *nick);
gboolean buddy_set_realname_by_fp(gchar *fingerprint, gchar *realname);
gboolean buddy_set_keyverified_by_fp(const char *fingerprint, const gint verified);
gboolean buddy_set_languages_by_fp(gchar *fingerprint, gchar *languages);
gboolean buddy_set_pref_contact_by_fp(gchar *fingerprint, gchar *pref_contact);
gboolean buddy_set_timezone_by_fp(gchar *fingerprint, gchar *timezone);
gboolean buddy_set_geolocation_by_fp(gchar *fingerprint, gchar *geolocation);
gboolean buddy_set_clientid_by_fp(gchar *fingerprint, SilcClientID *clientid);
gboolean buddy_remove_by_fp(gchar *fingerprint);

gchar *buddy_get_notes_by_fp(gchar *fingerprint);
gboolean buddy_set_notes_by_fp(gchar *fingerprint, gchar *notes);

gboolean buddy_update(gchar *str_fingerprint, gchar *nickname, 	 
		      gchar *realname, SilcClientID *id);

#endif
