#ifndef QUERY_H__
#define QUERY_H__

#include <glade/glade.h> /* for GladeXML */
#include <glib.h> /* for gpointer */

struct query_t {
    struct query_t	*next;
    SilcClientEntry	client_entry;

    guint		pagenr;

    GladeXML		*glade_xml;
    gpointer		*label_text;
    gpointer		*label_image;
};

typedef struct query_t SilkyQuery;

SilkyQuery *query_find_by_cliententry( SilcClientEntry cliententry );
SilkyQuery *query_find_by_pagenr( guint pagenr );

SilkyQuery *query_add( SilcClientEntry cliententry );

void query_pagenr_set_by_cliententry( SilcClientEntry cliententry, int pagenr );

gboolean query_remove( SilkyQuery *query );
gboolean query_remove_by_cliententry( SilcClientEntry cliententry );
gboolean query_remove_by_pagenr( guint pagenr );

void query_free_all();


#endif
