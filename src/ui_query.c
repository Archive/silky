/*
    Silky - A GTK+ client for SILC.
    Copyright (C) 2003, 2004, 2005 Toni Willberg

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    http://silky.sourceforge.net/

*/

#include <gtk/gtk.h>
#include <glade/glade.h>

#include <silcincludes.h>
#include <silcclient.h>

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "ui_query.h"
#include "support.h"
#include "preferences.h"
#include "silkygtkentry.h"

#include "log.h"
#include "gui.h"

extern GladeXML *xmlmain;
extern SilkyQuery *query_active;
extern silkyStruct *silky;
extern gchar *glade_silky_query;
extern gchar *glade_silky_query_label;

/* creates new query window or returns an old one if such exists */
void * get_query_window (SilcClientID *clientid) {
  unsigned char *str_clientid;
  GtkNotebook *notebook;
  GtkWidget *widget_query, *inputbox;
  SilcClientEntry cliententry;
  GladeXML *xml_query;
  GtkWindow *window_query;
  GtkLabel *label_nickname;
  GtkLabel *label_realname;
  GtkWidget *widget_tabhbox;
  GtkWidget *wid_tab_label = NULL;
  GtkWidget *stored_widget;
  SilkyQuery *query = NULL;

  debug("get_query_window()");

  if (!clientid) {
    debug("Did not get clientid at get_query_window() ");
    return NULL;
  }

  str_clientid =  silc_id_id2str(clientid, SILC_ID_CLIENT);
  if (!str_clientid) {
    debug("did not find str_clientid at get_query_window() ");
    return NULL;
  }

  cliententry = silc_client_get_client_by_id(silky->client, silky->conn, clientid);
  if (!cliententry) {
    debug("Did not get cliententry at get_query_window(). Returning. ");
    return NULL;
  }

  /* get notebook widget */
  notebook = GTK_NOTEBOOK(glade_xml_get_widget (xmlmain, "tabs"));

  /* fetch query of this user, if exists */
  query = query_find_by_cliententry(cliententry);

  //  if (!widget_query) {
  if (!query) {
    debug("No existing query widget found for '%s', creating new...", cliententry->nickname);


    query = query_add( cliententry ); /* allocate new SilkyQuery */



    /* get xml for query */
    xml_query = glade_xml_new (glade_silky_query, NULL, NULL);
    if (!xml_query) {
      g_error("Can not find the silky-query.glade file.");
    }

    query->glade_xml = xml_query;

    /* connect signals */
    glade_xml_signal_autoconnect (xml_query);

    /* get parent widget */
    window_query = GTK_WINDOW(glade_xml_get_widget (xml_query, "window_query"));
    /* get query widget */
    widget_query = glade_xml_get_widget (xml_query, "query_hbox");

    /* create a SilkyGtkEntry for the inputbox */
    inputbox = silky_gtk_entry_new(atoi(prefs_get("command_history_depth")));
    g_signal_connect(inputbox, "activate", G_CALLBACK(on_ui_send_check), NULL);
    gtk_box_pack_start(GTK_BOX(glade_xml_get_widget(xml_query, "inputhbox")), inputbox, 1, 1, 0);

    /* set nickname and realname of the queried user */
    label_nickname =  GTK_LABEL(glade_xml_get_widget (xml_query, "query_nickname"));
    label_realname =  GTK_LABEL(glade_xml_get_widget (xml_query, "query_realname"));

    gtk_label_set_text(label_nickname, cliententry->nickname);
    if (g_utf8_validate(cliententry->realname, -1, NULL)) {
      gtk_label_set_text(label_realname, cliententry->realname);
    }

    /* DON'T try to use stored data before this line! :) */

    /* store pointer to the widget_query by str_client to the notebook object */
    g_object_set_data(G_OBJECT(notebook), (gchar *)str_clientid, widget_query);

    /* store the clientid into the widget */
    g_object_set_data(G_OBJECT(widget_query), "clientid", clientid);

    g_object_set_data(G_OBJECT(widget_query), "cliententry", cliententry);

    /* store tab type */
    g_object_set_data(G_OBJECT(widget_query), "tab_type", (gpointer *)TAB_TYPE_QUERY);

    /* destroy unnecessary parent */
    g_object_ref(widget_query);
    gtk_container_remove(GTK_CONTAINER(window_query), widget_query);


    /* query text widget, temporary */
    stored_widget = glade_xml_get_widget (xml_query, "textview");

    /* set all color and font tags */
    set_textview_colors(GTK_TEXT_VIEW(stored_widget) );


    /* generate tab label stuff and pack it */
    widget_tabhbox = gtk_hbox_new(FALSE, 2);
    query->label_text = (gpointer)gtk_label_new(cliententry->nickname);
    query->label_image = (gpointer)gtk_image_new_from_stock( GTK_STOCK_JUSTIFY_FILL, GTK_ICON_SIZE_BUTTON );
    gtk_box_pack_start(GTK_BOX(widget_tabhbox), (gpointer)query->label_image, TRUE, TRUE, 0);
    gtk_box_pack_end(GTK_BOX(widget_tabhbox), (gpointer)query->label_text, TRUE, TRUE, 0);


    /* create new page/tab to the notebook */
    gtk_notebook_append_page(notebook, widget_query, widget_tabhbox); /* create new tab */
    query->pagenr = gtk_notebook_get_n_pages(notebook)-1;

    gtk_widget_show_all(widget_tabhbox);

    /* Store widget of tab label for later user, FIXME: this can(?) be removed after query struct is done */
    g_object_set_data(G_OBJECT(widget_query), "tablabel", wid_tab_label);

    g_object_unref(widget_query);

  }
  else {
    debug("Found existing query widget, using it...");
  }

  /* ACTIVATE current page */
  //  gtk_notebook_set_current_page(notebook, gtk_notebook_page_num(notebook, widget_query));
//  gtk_notebook_set_current_page(notebook, query->pagenr);
  //  stored_query_xml = glade_get_widget_tree(widget_query);
  // stored_widget = glade_xml_get_widget (stored_query_xml, "textview");
  stored_widget = glade_xml_get_widget (query->glade_xml, "textview");

  debug("\treturning the widget");

  return stored_widget;

}


/*
   This prints given message to the QUERY tab.
   args: target clientid tab, printed nickname of target/sender, flags and message string
*/
int printquery (SilcClientID *clientid, gchar *nickname, SilcMessageFlags flag, const char *str) {
  GtkTextView *textview;
  GtkTextBuffer *buffer;
  GtkTextIter iter; /* pointer to current location */
  gchar *timestamp;
  gchar *outstring;
  gchar *nickstring;
  gchar *flagstring;
  SilcClientEntry cliententry;

  debug("printquery()");

  textview = GTK_TEXT_VIEW(get_query_window(clientid));

  if (!textview) {
    debug("Can not find text view!");
    return FALSE;
  }

  buffer = GTK_TEXT_BUFFER(gtk_text_view_get_buffer(textview));

  if (!buffer) {
    debug("Can not find text buffer!");
    return FALSE;
  }

  /* Get the end of the buffer */
  gtk_text_buffer_get_end_iter(buffer, &iter);

  timestamp = g_strdup_printf("[%s] ", utf_timestamp() );
  outstring = g_strdup_printf("%s\n", str); /* append \n */

  /* was it an action? */
  if ((flag & SILC_MESSAGE_FLAG_ACTION)) {
    debug("flag: SILC_MESSAGE_FLAG_ACTION");
    nickstring = g_strdup_printf("* %s ", nickname); /* *me actions */
  }
  else {
    nickstring = g_strdup_printf("<%s> ", nickname); /* me: says */
  }

  /* was it signed message? */
  if (flag & SILC_MESSAGE_FLAG_SIGNED) {
    debug("flag: SILC_MESSAGE_FLAG_SIGNED");
    flagstring = g_strdup("[S]");
  }
  else {
    flagstring = g_strdup("");
  }


  /* timestamp */
  gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, timestamp, -1, "timestamp", NULL);

  /* flagstring */
  gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, flagstring, -1, "nickname", NULL);

  /* nickname */
  gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, nickstring, -1, "nickname", NULL);

  /* Print the text */
  gtk_text_buffer_insert_with_tags_by_name (buffer, &iter, outstring, -1, "message", NULL);

  gtk_text_buffer_get_end_iter(buffer, &iter);
  gtk_text_buffer_create_mark(buffer, "end_mark", &iter, FALSE);
  gtk_text_view_scroll_to_mark(textview, gtk_text_buffer_get_mark(buffer, "end_mark"), 0.1, FALSE, 0,1);

  g_free(timestamp);
  g_free(outstring);
  g_free(flagstring);
  g_free(nickstring);

  cliententry = silc_client_get_client_by_id(silky->client, silky->conn, clientid);

  hilight_query_window_by_q(query_find_by_cliententry(cliententry), HILIGHT_SPEECH);

  return TRUE;
}

