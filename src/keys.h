#ifndef KEYS_H__
#define KEYS_H__

/*
 all key-related stuff lives here
*/



typedef struct {
  gchar *name;
  gchar *email;
  gchar *passphrase;
  gint length;
  gchar *cipher;
  gchar *identifier;
  gboolean ready; /* keygen makes this TRUE when the key is generated */
} SilkySilcKey;


void silky_keygen (SilkySilcKey *key);
gboolean silky_save_public_key(SilcClient client, SilcClientConnection conn,
                                const char *name, SilcSocketType conn_type, 
                                unsigned char *pk, SilcUInt32 pk_len, 
                                SilcSKEPKType pk_type,
                                SilcVerifyPublicKey completion, void *context);

gboolean silky_known_server_key(SilcPublicKey pkey);
gboolean silky_known_client_key(SilcPublicKey pkey);

gint silky_keygen_ui ();
const gchar *gen_keyfile_path(const gchar *fingerprint, const gchar *key_type);

enum {
    SILC_MSG_SIGNED_VERIFIED,
    SILC_MSG_SIGNED_UNKNOWN,
    SILC_MSG_SIGNED_FAILED
} SilcMessageSignatureStatus;

#endif
