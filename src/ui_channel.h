#ifndef UI_CHANNEL_H__
#define UI_CHANNEL_H__

#include "channel.h"

void populate_gui_nicklist(struct channel_t*);
void refresh_gui_nicklist(struct channel_t*);
void silky_colorize_nicklist (GtkWidget*);
void channel_tab_completion( void );
void channel_reset_tab_completion( void );
gboolean on_topic_restore(GtkWidget *widget, gpointer user_data);
gboolean on_topic(GtkWidget *widget, gpointer user_data);
char * get_active_channel_name (void);
SilcChannelID * get_active_channel_id (void);
GtkWidget * get_stored_channel_widget (const char *channel_name, const char *widget_name);
GtkWidget * get_stored_channel_window (const char *channel_name);
int print_mime_to_channel (SilcClientEntry client_entry, const char *channel_name, int message_signed, const gchar *content_type, ...);

int printchannel (const char *channel_name, const char *str);
void error_no_channel_oper_priv (gchar *channel_name);
void error_no_channel_founder_priv (gchar *channel_name);
void error_user_not_on_channel (gchar *channel_name);
gchar * set_cmode_window_widgets (gchar *channel_name, SilcUInt32 mode);
void on_nicklist_selected (GtkTreeSelection *selection, gpointer data);
void on_nicklist_activated (GtkTreeView *treeview, GtkTreePath *arg1, GtkTreeViewColumn *arg2, gpointer user_data);
gint on_nick_pressed (GtkTreeView *widget, GdkEventButton *event);

typedef enum {
    USERLIST_COL_FOUNDER,
    USERLIST_COL_CHANOP,
    USERLIST_COL_NICKNAME,
    USERLIST_COL_CLIENTID,
    USERLIST_COL_STRCLIENTID
} SilkyChannelUserlistColumns;

#endif
