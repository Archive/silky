void silc_command_reply(SilcClient client, SilcClientConnection conn,
			SilcCommandPayload cmd_payload, bool success,
			SilcCommand command, SilcStatus status, ...);
