#include <gtk/gtk.h>
#include "silcincludes.h"
#include "silcclient.h"

gint on_command_whois(GtkMenuItem *menuitem, gpointer clientid);
gint on_command_whois_pubkey(GtkMenuItem *, gpointer);
gint on_command_cumode(GtkMenuItem *menuitem, gpointer clientid);
gint on_command_kill(GtkMenuItem *menuitem, gpointer clientid);
gint on_command_kick(GtkMenuItem *menuitem, gpointer clientid);
gint on_command_query(GtkMenuItem *menuitem, gpointer clientid);


int on_window_hide(GtkWidget *widget, gpointer user_data);

void on_session_resume(GtkWidget *widget, gpointer user_data);
void on_session_detach(GtkWidget *widget, gpointer user_data);

void on_server_connect(GtkWidget *widget, gpointer user_data);

void on_whoami(GtkWidget *widget, gpointer user_data);

void on_settings(GtkWidget *widget, gpointer user_data);
int on_settings_close(GtkWidget *widget, gpointer user_data);
void on_settings_save(GtkWidget *widget, gpointer user_data);
void on_settings_cancel(GtkWidget *widget, gpointer user_data);

void on_settings_create_new_key(GtkWidget *widget, gpointer user_data);
void on_settings_create_new_key_create(GtkWidget *widget, gpointer user_data);

void on_show_new_query_window(GtkWidget *widget, gpointer user_data);
void on_show_join_channel_window(GtkWidget *widget, gpointer user_data);

void on_join_channel(GtkWidget *widget, gpointer user_data);
void on_join_channel_invited(GtkWidget *widget, gpointer user_data);
void on_new_query(GtkWidget *widget, gpointer user_data);
void on_new_query_resolved(SilcClient client, SilcClientConnection conn, SilcClientEntry *clients, SilcUInt32 clients_count, void *context);
void on_new_query_activated(GtkTreeView *treeview, GtkTreePath *arg1, GtkTreeViewColumn *arg2, gpointer user_data);
void on_leave_query(GtkWidget *widget, gpointer user_data);
void on_leave_channel(GtkWidget *widget, gpointer user_data);


void on_ui_help(GtkWidget *widget, gpointer user_data);
void on_ui_find(GtkWidget *widget, gpointer user_data);
void on_ui_redo(GtkWidget *widget, gpointer user_data);
void on_ui_clear(GtkWidget *widget, gpointer user_data);
void on_ui_quit(GtkWidget *widget, gpointer user_data);
void on_ui_about(GtkWidget *widget, gpointer user_data);
void on_ui_cut(GtkWidget *widget, gpointer user_data);
void on_ui_copy(GtkWidget *widget, gpointer user_data);
void on_ui_paste(GtkWidget *widget, gpointer user_data);
void on_ui_undo(GtkWidget *widget, gpointer user_data);

void on_ui_close_tab(GtkWidget *widget, gpointer user_data);
void on_ui_send(GtkWidget *widget, gpointer user_data);

void on_ui_tabs_switch_page(GtkNotebook *notebook, GtkNotebookPage *page,
			    guint page_num, gpointer user_data);

void on_umode_block_private_messages(GtkWidget *widget, gpointer user_data);
void on_umode_await_paging(GtkWidget *widget, gpointer user_data);
void on_umode_busy(GtkWidget *widget, gpointer user_data);
void on_umode_hyperactive(GtkWidget *widget, gpointer user_data);

void on_message_open(GtkWidget *widget, gpointer user_data);


void on_show_send_file_dialog(GtkWidget *widget, gpointer user_data);
void on_send_file_from_dialog(GtkWidget *widget, gpointer user_data);

void on_show_server_connect_window(GtkWidget *widget, gpointer user_data);
void on_show_nick_change_window(GtkWidget *widget, gpointer user_data);
gboolean on_nick_change(GtkWidget *widget, gpointer user_data);

gboolean on_cmode_menu(GtkWidget *widget, gpointer user_data);
gboolean on_disconnect_from_server(GtkWidget *widget, gpointer data);


void on_serverinfo(GtkWidget *widget, gpointer user_data);


void on_favourite_server_add(GtkWidget *widget, gpointer user_data);
void on_favourite_person_add(GtkWidget *widget, gpointer user_data);
void on_favourite_channel_add(GtkWidget *widget, gpointer user_data);

void on_umode_away(GtkWidget *widget, gpointer user_data);
void on_umode_reject_watching(GtkWidget *widget, gpointer user_data);
void on_umode_block_invites(GtkWidget *widget, gpointer user_data);
void on_umode_indisposed(GtkWidget *widget, gpointer user_data);

gboolean on_key_press(GtkWidget *widget, GdkEventKey *key, gpointer user_data);

void on_new_msg_resolved(SilcClient client, SilcClientConnection conn, SilcClientEntry *clients, SilcUInt32 clients_count, void *context);
/* void handle_multiline (char *cmd, int history, int nocommand);*/

gboolean *on_received_invite_user_info(GtkWidget *widget, SilcClientEntry client_entry);
